//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the data model for the chef order items                   *
//*                                                                                                            *
//*                                        Saved in: ModelChefOrderItem.java                                   *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.client;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.pizza.data.ItemDetail;
import com.pizza.data.OrderItem;

public class ModelChefOrderItem extends AbstractTableModel {
	private List<ItemDetail> orderItemDetailList = new ArrayList<ItemDetail>();

    private final String[] headers = {"Item Id", "Name", "Type", "Size"};


    public ModelChefOrderItem(List<ItemDetail> orderItemDetailList) {
        super();
        this.orderItemDetailList = orderItemDetailList;
    }

    public int getRowCount() {
        return this.orderItemDetailList.size();
    }

    public int getColumnCount() {
        return headers.length;
    }

    public String getColumnName(int columnIndex) {
        return headers[columnIndex];
    }
    
    @Override
    public Class getColumnClass(int columnIndex) {
    	switch(columnIndex) {
    	case 0: 
        	return Integer.class;
    	case 1: 
    		return String.class;
    	case 2:
    		return String.class;
    	case 3:
    		return String.class;
    	default:
    		return Object.class;
    	}
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
    	return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
        	case 0:
                return ((ItemDetail) this.orderItemDetailList.get(rowIndex)).getItemId();
            case 1:
                return ((ItemDetail) this.orderItemDetailList.get(rowIndex)).getName();
            case 2:
                return ((ItemDetail) this.orderItemDetailList.get(rowIndex)).getType();
            case 3:
                return ((ItemDetail) this.orderItemDetailList.get(rowIndex)).getSize();
            default:
                return null; 
        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (aValue != null) {
        	ItemDetail itemDetail = (ItemDetail) this.orderItemDetailList.get(rowIndex);
        	switch(columnIndex){
        	case 0:
               itemDetail.setItemId((Integer)aValue);
               break;
        	case 1:
                itemDetail.setName((String)aValue);
                break;
        	case 2:
                itemDetail.setType((String)aValue);
                break;
        	case 3:
                itemDetail.setSize((String)aValue);
                break;
        	}
        }
    }

}
