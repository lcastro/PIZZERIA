//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the data model for the menu items                         *
//*                                                                                                            *
//*                                        Saved in: ModelMenuItem.java                                        *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.client;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import com.pizza.data.ItemDetail;

public class ModelMenuItem extends AbstractTableModel{

	private List<ItemDetail> ItemDetailList = new ArrayList<ItemDetail>();

    private final String[] headers = {"Select", "ID", "Name", "Type", "Size", "Price"};


    public ModelMenuItem(List<ItemDetail> itemDetailList) {
        super();
        this.ItemDetailList = itemDetailList;
    }

    public int getRowCount() {
        return this.ItemDetailList.size();
    }

    public int getColumnCount() {
        return headers.length;
    }

    public String getColumnName(int columnIndex) {
        return headers[columnIndex];
    }
    
    @Override
    public Class getColumnClass(int columnIndex) {
    	return (getValueAt(0, columnIndex).getClass());
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
    	return columnIndex == 0;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
        	case 0:
                return ((ItemDetail) this.ItemDetailList.get(rowIndex)).getSelection();
            case 1:
                return ((ItemDetail) this.ItemDetailList.get(rowIndex)).getItemId();
            case 2:
                return ((ItemDetail) this.ItemDetailList.get(rowIndex)).getName();
            case 3:
                return ((ItemDetail) this.ItemDetailList.get(rowIndex)).getType();
            case 4:
                return ((ItemDetail) this.ItemDetailList.get(rowIndex)).getSize();
            case 5:
                return ((ItemDetail) this.ItemDetailList.get(rowIndex)).getUnitPrice();
            default:
                return null; 
        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (aValue != null) {
        	ItemDetail itemDetail = (ItemDetail) this.ItemDetailList.get(rowIndex);
        	switch(columnIndex){
        	case 0:
               itemDetail.setSelection((Boolean)aValue);
               break;
        	}
        }
    }
    
}
