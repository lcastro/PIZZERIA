//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the data model for the orders                             *
//*                                                                                                            *
//*                                        Saved in: ModelOrder.java                                           *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.client;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.pizza.data.Order;

public class ModelOrder extends AbstractTableModel {
	private List<Order> orderList = new ArrayList<Order>();

    private final String[] headers = {"Select", "OrderId", "Date", "Status"};


    public ModelOrder(List<Order> orderList) {
        super();
        this.orderList = orderList;
    }

    public int getRowCount() {
        return this.orderList.size();
    }

    public int getColumnCount() {
        return headers.length;
    }

    public String getColumnName(int columnIndex) {
        return headers[columnIndex];
    }
    
    @Override
    public Class getColumnClass(int columnIndex) {
    	switch(columnIndex) {
    	case 0: 
        	return Boolean.class;
    	case 1: 
    		return Integer.class;
    	case 2:
    		return LocalDate.class;
    	case 3:
    		return String.class;
    	default:
    		return Object.class;
    	}
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
    	return columnIndex == 0;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
        	case 0:
                return ((Order) this.orderList.get(rowIndex)).getSelection();
            case 1:
                return ((Order) this.orderList.get(rowIndex)).getOrderId();
            case 2:
                return ((Order) this.orderList.get(rowIndex)).getOrderDate();
            case 3:
                return ((Order) this.orderList.get(rowIndex)).getOrderStatus();
            default:
                return null; 
        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (aValue != null) {
        	Order order = (Order) this.orderList.get(rowIndex);
        	switch(columnIndex){
        	case 0:
               order.setSelection((Boolean)aValue);
               break;
        	case 1:
                order.setOrderId((Integer)aValue);
                break;
        	case 2:
                order.setOrderDate((LocalDate)aValue);
                break;
        	case 3:
                order.setOrderStatus((String)aValue);
                break;
        	}
        }
    }

}
