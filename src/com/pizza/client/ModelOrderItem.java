//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the data model for the order items                        *
//*                                                                                                            *
//*                                        Saved in: ModelOrderItem.java                                       *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.client;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import com.pizza.data.OrderItem;

public class ModelOrderItem extends AbstractTableModel {
	private List<OrderItem> orderItemList = new ArrayList<OrderItem>();

    private final String[] headers = {"Select", "Item", "Qty"};


    public ModelOrderItem(List<OrderItem> orderItemList) {
        super();
        this.orderItemList = orderItemList;
    }

    public int getRowCount() {
        return this.orderItemList.size();
    }

    public int getColumnCount() {
        return headers.length;
    }

    public String getColumnName(int columnIndex) {
        return headers[columnIndex];
    }
    
    @Override
    public Class getColumnClass(int columnIndex) {
    	switch(columnIndex) {
    	case 0: 
        	return Boolean.class;
    	case 1: 
    		return String.class;
    	case 2:
    		return Integer.class;
    	default:
    		return Object.class;
    	}
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
    	switch(columnIndex) {
    	case 0: 
    		return true;
    	case 1:
    		return false;
    	case 2:
    		return true;
    	default:
    		return false;
    	}
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
        	case 0:
                return ((OrderItem) this.orderItemList.get(rowIndex)).getSelection();
            case 1:
                return ((OrderItem) this.orderItemList.get(rowIndex)).getItem();
            case 2:
                return ((OrderItem) this.orderItemList.get(rowIndex)).getQty();
            default:
                return null; 
        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (aValue != null) {
        	OrderItem orderItem = (OrderItem) this.orderItemList.get(rowIndex);
        	switch(columnIndex){
        	case 0:
               orderItem.setSelection((Boolean)aValue);
               break;
        	case 1:
                orderItem.setItem((String)aValue);
                break;
        	case 2:
                orderItem.setQty((Integer)aValue);
                break;
        	}
        }
    }

}
