//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the order status as a ENUM type                           *
//*                                                                                                            *
//*                                        Saved in: OrderStatus.java                                          *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.client;

public enum OrderStatus {
	PLACED,
	RECEIVED,
	INPROCESS,
	COMPLETED
}
