//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the editor to allow the chef update                       *
//*                            an order status in the table view                                               *
//*                                                                                                            *
//*                                        Saved in: OrderStatusCellEditor.java                                *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.client;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;

public class OrderStatusCellEditor extends DefaultCellEditor { 
	public OrderStatusCellEditor() {
		super(new JComboBox(OrderStatus.values()));
	}
}
