//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the Chef UI and functions                                 *
//*                                                                                                            *
//*                                        Saved in: PizzaChef.java                                            *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.client;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.pizza.data.ItemDetail;
import com.pizza.data.ItemList;
import com.pizza.data.Order;
import com.pizza.data.Payment;
import com.pizza.client.OrderStatusCellEditor;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class PizzaChef extends JPanel {
	private JTable tblOrders;
	private JTable tblOrderItems;
	private List<Order> orderList;
	private List<ItemList> itemListList;
	private List<ItemDetail> orderItemDetailList;
	private JButton btnLogout;
	private JButton btnUpdateOrder;
	private JButton btnViewOrderItems;
	
	
	PizzaClient connection;

	/**
	 * Create the panel.
	 */
	public PizzaChef(PizzaClient connection) {
		setBackground(new Color(222, 184, 135));
		this.connection = connection;
		this.retrieveOrderList();
		this.initializaComponents();
		this.setActionListeners();
		this.orderItemDetailList = new ArrayList<ItemDetail>();
	}
	
	private void retrieveOrderList() {
		try {
			connection.message.setOpType(connection.SELECT_ORDERS_OP);
			connection.sendMessage();
			this.orderList = connection.message.getOrderList();

			connection.message.setOpType(connection.RETRIEVE_USR_OP);
			for (int i = 0; i < orderList.size(); i++) {
				Order orderItem = orderList.get(i);
            	connection.message.setUser(orderItem.getUser());
            	connection.sendMessage();
            	orderItem.setUser(connection.message.getUser());
            	orderList.set(i, orderItem);
			}
		} catch (Exception ex) {
			String message = "An error occurred during the selection of order list";
			JOptionPane.showMessageDialog(null, message);
			ex.printStackTrace();
		}
	}
	
	private void initializaComponents() {
		JScrollPane scrOrders = new JScrollPane();
		
		JLabel lblChefOrders = new JLabel("Chef Orders");
		lblChefOrders.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		JLabel lblOrders = new JLabel("Orders");
		lblOrders.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JLabel lblOrderItems = new JLabel("Order Items");
		lblOrderItems.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JScrollPane scrOrderItems = new JScrollPane();
		
		btnLogout = new JButton("Logout");
		
		btnUpdateOrder = new JButton("Update Order");
		
		btnViewOrderItems = new JButton("View Order Items");
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(136, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(217)
							.addComponent(lblChefOrders))
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
							.addComponent(lblOrders)
							.addComponent(scrOrders, GroupLayout.DEFAULT_SIZE, 573, Short.MAX_VALUE)
							.addGroup(groupLayout.createSequentialGroup()
								.addComponent(lblOrderItems)
								.addGap(258))
							.addGroup(groupLayout.createSequentialGroup()
								.addGap(237)
								.addComponent(btnLogout, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
							.addComponent(scrOrderItems))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(148)
							.addComponent(btnViewOrderItems)
							.addGap(34)
							.addComponent(btnUpdateOrder, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)))
					.addGap(121))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(33)
					.addComponent(lblChefOrders)
					.addGap(13)
					.addComponent(lblOrders)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrOrders, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnUpdateOrder)
						.addComponent(btnViewOrderItems))
					.addGap(18)
					.addComponent(lblOrderItems)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrOrderItems, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnLogout)
					.addContainerGap(73, Short.MAX_VALUE))
		);
		
		//Order Items
		tblOrderItems = new JTable();
		tblOrderItems.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Item Id", "Name", "Type", "Size"
			}
		));
		tblOrderItems.getColumnModel().getColumn(1).setPreferredWidth(255);
		tblOrderItems.getColumnModel().getColumn(2).setPreferredWidth(149);
		tblOrderItems.getColumnModel().getColumn(3).setPreferredWidth(131);
		scrOrderItems.setViewportView(tblOrderItems);
		
		//Orders
		tblOrders = new JTable(new ModelChefOrder(orderList));
		tblOrders.setDefaultEditor(OrderStatus.class, new OrderStatusCellEditor());
//		tblOrders = new JTable();
//		tblOrders.setModel(new DefaultTableModel(
//			new Object[][] {
//			},
//			new String[] {
//				"Select", "OrderId", "Date", "Customer", "Qty", "Status"
//			}
//		));
		tblOrders.getColumnModel().getColumn(0).setPreferredWidth(53);
		tblOrders.getColumnModel().getColumn(3).setPreferredWidth(206);
		tblOrders.getColumnModel().getColumn(5).setPreferredWidth(143);
		scrOrders.setViewportView(tblOrders);
		setLayout(groupLayout);
	}	
	
	private void setActionListeners() {
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connection.changeState(ViewState.LOGIN_STATE);
			}
		});
	
		btnUpdateOrder.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					for(Order order : orderList){
						if (order.getSelection()) {
							connection.message.setOrderStatus(order.getOrderStatus());
							connection.message.setOrder(order);
							break;
						}
					}
					connection.message.setOpType(connection.UPDATE_ORDER_STATUS_OP);
	            	connection.sendMessage();
				} catch (Exception ex) {
					String message = "An error occurred during the order status update";
					JOptionPane.showMessageDialog(null, message);
					ex.printStackTrace();
				}
			}
			
		});
		
		btnViewOrderItems.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				orderItemDetailList.clear();
				for (Order order : orderList) {
					if (order.getSelection()) {
						try {
							connection.message.setOrder(order);
							connection.message.setOpType(connection.RETRIEVE_ITEM_LIST_OP);
							connection.sendMessage();
							itemListList = connection.message.getItemListList();

							for (ItemList itemList : itemListList) {
								ItemDetail itemDetail = new ItemDetail();
								itemDetail.setItemId(itemList.getItemDetailItemId());
								connection.message.setItemDetail(itemDetail);
								connection.message.setOpType(connection.VIEW_ITEM_DETAIL_OP);
								connection.sendMessage();
								orderItemDetailList.add(connection.message.getItemDetail());
							}
						} catch (Exception ex) {
							String message = "An error occurred during the selection of order list";
							JOptionPane.showMessageDialog(null, message);
							ex.printStackTrace();
						}
						tblOrderItems.setModel(new ModelChefOrderItem(orderItemDetailList));
						break;
					}
				}
			}
		});
		
	}
}
