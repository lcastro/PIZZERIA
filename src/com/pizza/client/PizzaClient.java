//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the main client application                               *
//*                                                                                                            *
//*                                        Saved in: PizzaClient.java                                          *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.client;

import java.awt.Container;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.pizza.data.ItemDetail;
import com.pizza.data.ItemList;
import com.pizza.data.Message;
import com.pizza.data.Order;
import com.pizza.data.Payment;
import com.pizza.data.User;

import javax.swing.*;

class PizzaClient {

	private JPanel pizzaLogin;
	private JPanel pizzaUser;
	private JPanel pizzaOrder;
	private JPanel pizzaItem;
	private JPanel pizzaPayment;
	private JPanel pizzaChef;
	private JFrame frame;
	private ViewState viewState;
	
	private String hostname;
	private int port;
	private Socket conn;
	
	public Message message;
	
	// IO streams
	public ObjectOutputStream clientOutputStream;
	public ObjectInputStream clientInputStream;

	public final int RETRIEVE_USR_OP = 100;
	public final int INSERT_USR_OP = 110;
	public final int UPDATE_USR_OP = 120;
	public final int DELETE_USR_OP = 130;
	public final int VALIDATE_USR_OP = 140;

	public final int VIEW_ORDER_OP = 200;
	public final int INSERT_ORDER_OP = 210;
	public final int UPDATE_ORDER_OP = 220;
	public final int DELETE_ORDER_OP = 230;
	public final int SELECT_ALL_ORDERS_OP = 240;
	public final int SELECT_ORDERS_OP = 250;
	public final int UPDATE_ORDER_STATUS_OP = 260;

	public final int VIEW_PAYMENT_OP = 300;
	public final int INSERT_PAYMENT_OP = 310;
	public final int UPDATE_PAYMENT_OP = 320;
	public final int DELETE_PAYMENT_OP = 330;

	public final int VIEW_ITEM_LIST_OP = 400;
	public final int INSERT_ITEM_LIST_OP = 410;
	public final int UPDATE_ITEM_LIST_OP = 420;
	public final int DELETE_ITEM_LIST_OP = 430;
	public final int RETRIEVE_ITEM_LIST_OP = 440;

	public final int VIEW_ITEM_DETAIL_OP = 500;
	public final int INSERT_ITEM_DETAIL_OP = 510;
	public final int UPDATE_ITEM_DETAIL_OP = 520;
	public final int DELETE_ITEM_DETAIL_OP = 530;
	public final int SELECT_ALL_ITEM_DETAIL_OP = 540;

	public final String SUCCESS = "S";
	public final String FAILURE = "F";
	
	public PizzaClient(String hostname, int port) throws IOException {

		this.port = port;
		this.hostname = hostname;

		// Create a connection with the StaffServer server on port number 8000
		conn = new Socket(this.hostname, this.port);
		clientOutputStream = new ObjectOutputStream(conn.getOutputStream());
		clientInputStream = new ObjectInputStream(conn.getInputStream());
		message = new Message();
		viewGui();
	}

	public void sendMessage() throws IOException, ClassNotFoundException {
		clientOutputStream.writeObject(message);
		receivingMeesage();
	}

	private void receivingMeesage() throws IOException, ClassNotFoundException {
		message = (Message) clientInputStream.readObject();

		switch (message.getOpType()) {
		case RETRIEVE_USR_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				changeState(ViewState.LOGIN_STATE);
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case INSERT_USR_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				changeState(ViewState.LOGIN_STATE);
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case VALIDATE_USR_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				User user = message.getUser();
				if (user.getRole().equals("CUSTOMER")) {
					changeState(ViewState.USER_ORDER_STATE);
				} else if (user.getRole().equals("CHEF")) {
					changeState(ViewState.CHEF_STATE);
				} else if (user.getRole().equals("ADMIN")) {
					changeState(ViewState.ADMIN_STATE);
				}
			} else {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case VIEW_ORDER_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Selected");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case INSERT_ORDER_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Inserted");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case UPDATE_ORDER_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Updated");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case DELETE_ORDER_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Deleted");
			} else {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case SELECT_ALL_ORDERS_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Orders Selected");
			} else {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case SELECT_ORDERS_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Orders Selected");
			} else {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case UPDATE_ORDER_STATUS_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Order Status Updated");
			} else {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case VIEW_PAYMENT_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Selected");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case INSERT_PAYMENT_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Inserted");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case UPDATE_PAYMENT_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Updated");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case DELETE_PAYMENT_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Deleted");
			} else {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case VIEW_ITEM_LIST_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Selected");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case INSERT_ITEM_LIST_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Inserted");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case UPDATE_ITEM_LIST_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Updated");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case DELETE_ITEM_LIST_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Deleted");
			} else {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case RETRIEVE_ITEM_LIST_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Retreive Item List");
			} else {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case VIEW_ITEM_DETAIL_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Selected");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case INSERT_ITEM_DETAIL_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Inserted");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case UPDATE_ITEM_DETAIL_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Updated");
			} else if (message.getOpStatus().equals(FAILURE)) {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case DELETE_ITEM_DETAIL_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("Record Deleted");
			} else {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		case SELECT_ALL_ITEM_DETAIL_OP:
			if (message.getOpStatus().equals(SUCCESS)) {
				System.out.println("All Item Detail Selected");
			} else {
				JOptionPane.showMessageDialog(null, message.getErrorMsg());
			}
			break;
		}
	}
	
	void viewGui() {
		
		pizzaLogin = new PizzaLogin(this);
		frame = new JFrame();
		frame.setTitle("Pizzeria Client");
		Container contentPane = frame.getContentPane();
		contentPane.add(pizzaLogin);
		frame.setContentPane(contentPane);
		frame.pack();
		frame.setVisible(true);
		frame.setSize(850,660);
	}
	
	public void changeState(ViewState state) {
		viewState = state;
		
		switch(state) {
		case LOGIN_STATE:
			pizzaLogin = new PizzaLogin(this);
			frame.getContentPane().removeAll();
			frame.getContentPane().add(pizzaLogin);
			frame.revalidate();
			frame.pack();
			frame.setSize(850,660);
			break;
		case USER_REG_STATE:
			pizzaUser = new PizzaUser(this);
			frame.getContentPane().removeAll();
			frame.getContentPane().add(pizzaUser);
			frame.revalidate();
			frame.pack();
			frame.setSize(850,660);
			break;
		case USER_ORDER_STATE:
			pizzaOrder = new PizzaOrder(this);
			frame.getContentPane().removeAll();
			frame.getContentPane().add(pizzaOrder);
			frame.revalidate();
			frame.pack();
			frame.setSize(850,660);
			break;
		case ADMIN_STATE:
			pizzaItem = new PizzaItem(this);
			frame.getContentPane().removeAll();
			frame.getContentPane().add(pizzaItem);
			frame.revalidate();
			frame.pack();
			frame.setSize(850,660);
			break;
		case CHEF_STATE:
			pizzaChef = new PizzaChef(this);
			frame.getContentPane().removeAll();
			frame.getContentPane().add(pizzaChef);
			frame.revalidate();
			frame.pack();
			frame.setSize(860,660);
			break;
		case PAYMENT_STATE:
			pizzaPayment = new PizzaPayment(this);
			frame.getContentPane().removeAll();
			frame.getContentPane().add(pizzaPayment);
			frame.revalidate();
			frame.pack();
			frame.setSize(860,660);
			break;
		default:
			break;
			
		}
	}

	/** Main method */
	public static void main(String[] args) {
		try {
			PizzaClient client = new PizzaClient("localhost", 8001);
		} catch (Exception ex) {
			String message = "An error occurred in the StaffClient application - the system will exit.";
			JOptionPane.showMessageDialog(null, message);
			ex.printStackTrace();
			System.exit(0);
		}
	}
}
