//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the Admin UI and functions for                            *
//*                            menu items                                                                      *
//*                                                                                                            *
//*                                        Saved in: PizzaItem.java                                            *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.client;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import com.pizza.data.ItemDetail;
import com.pizza.data.User;

import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class PizzaItem extends JPanel {
	PizzaClient connection;
	ItemDetail itemDetail;
	
	private JTextField txtItemID;
	private JTextField txtName;
	private JTextField txtPrice;
	private JButton btnView;
	private JButton btnAdd;
	private JButton btnModify;
	private JButton btnDelete;
	private JButton btnLogout;
	private JComboBox cmbType;
	private JComboBox cmbSize;
	
	private String[] typeList = { "Cheese Pizza", "Classic Recipe Pizza", "Skinny Pizza", "Gluten-Free Pizza" };
	private String[] sizeList = { "Small", "Medium", "Large", "Personal" };
	
	/**
	 * Create the panel.
	 */
	public PizzaItem(PizzaClient connection) {
		setBackground(new Color(222, 184, 135));
		this.connection = connection;
		this.initializeComponents();
		this.setActionListeners();
	}

	private void initializeComponents() {
		setLayout(null);
		
		JLabel lblMenuItem = new JLabel("Menu Item");
		lblMenuItem.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblMenuItem.setBounds(327, 123, 126, 31);
		add(lblMenuItem);
		
		JLabel lblItemId = new JLabel("Item ID:");
		lblItemId.setBounds(229, 187, 46, 14);
		add(lblItemId);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(229, 223, 46, 14);
		add(lblName);
		
		JLabel lblType = new JLabel("Type:");
		lblType.setBounds(229, 263, 46, 14);
		add(lblType);
		
		JLabel lblSize = new JLabel("Size:");
		lblSize.setBounds(229, 305, 46, 14);
		add(lblSize);
		
		JLabel lblPrice = new JLabel("Price:");
		lblPrice.setBounds(229, 346, 46, 14);
		add(lblPrice);
		
		txtItemID = new JTextField();
		txtItemID.setBounds(335, 184, 86, 20);
		add(txtItemID);
		txtItemID.setColumns(10);
		
		txtName = new JTextField();
		txtName.setBounds(335, 220, 242, 20);
		add(txtName);
		txtName.setColumns(10);
		
		cmbType = new JComboBox(typeList);
		cmbType.setBounds(335, 260, 157, 20);
		cmbType.setSelectedIndex(0);
		add(cmbType);
		
		cmbSize = new JComboBox(sizeList);
		cmbSize.setBounds(335, 302, 157, 20);
		cmbSize.setSelectedIndex(0);
		add(cmbSize);
		
		txtPrice = new JTextField();
		txtPrice.setBounds(335, 343, 86, 20);
		add(txtPrice);
		txtPrice.setColumns(10);
		
		btnView = new JButton("View");
		btnView.setBounds(135, 424, 89, 23);
		add(btnView);

		btnAdd = new JButton("Add");
		btnAdd.setBounds(249, 424, 89, 23);
		add(btnAdd);

		btnModify = new JButton("Modify");
		btnModify.setBounds(366, 424, 89, 23);
		add(btnModify);
		
		btnDelete = new JButton("Delete");
		btnDelete.setBounds(480, 424, 89, 23);
		add(btnDelete);
		
		btnLogout = new JButton("Logout");
		btnLogout.setBounds(590, 424, 89, 23);
		add(btnLogout);
		
	}
	
	private void setActionListeners() {
		btnView.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					viewItemDetail();
				} catch (Exception ex) {
					String message = "An error occurred during the view item detail operation.";
					JOptionPane.showMessageDialog(null, message);
					ex.printStackTrace();
				}
			}
			
		});
		
		btnAdd.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					addItemDetail();
					clearItemDetailFields();
				} catch (Exception ex) {
					String message = "An error occurred during the add item detail operation.";
					JOptionPane.showMessageDialog(null, message);
					ex.printStackTrace();
				}
			}
			
		});
		
		btnModify.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					modifyItemDetail();
					clearItemDetailFields();
				} catch (Exception ex) {
					String message = "An error occurred during the modify item detail operation.";
					JOptionPane.showMessageDialog(null, message);
					ex.printStackTrace();
				}
			}
			
		});
		
		btnDelete.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					deleteItemDetail();
					clearItemDetailFields();
				} catch (Exception ex) {
					String message = "An error occurred during the delete item detail operation.";
					JOptionPane.showMessageDialog(null, message);
					ex.printStackTrace();
				}
			}
			
		});

		btnLogout.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				connection.changeState(ViewState.LOGIN_STATE);
			}

		});
		
	}
	
	private void viewItemDetail() throws Exception {
		String itemId = txtItemID.getText();
		
		itemDetail  = new ItemDetail();
		itemDetail.setItemId(Integer.parseInt(itemId));
		connection.message.setItemDetail(itemDetail);
		connection.message.setOpType(connection.VIEW_ITEM_DETAIL_OP);
		connection.sendMessage();
		
		itemDetail = connection.message.getItemDetail();
		txtItemID.setText(String.valueOf(itemDetail.getItemId()));
		txtName.setText(itemDetail.getName());
		//cmbType.
		
		int index = 0;
		String type = itemDetail.getType();
		if(Arrays.asList(typeList).contains(type)) {
			for(int i =0; i < typeList.length; i++) {
				if (typeList[i].equals(type)) {
					index = i;
					break;
				}
			}
			cmbType.setSelectedIndex(index);
		}
		
		index = 0;
		String size = itemDetail.getSize();
		if(Arrays.asList(sizeList).contains(size)) {
			for(int i =0; i < sizeList.length; i++) {
				if (sizeList[i].equals(size)) {
					index = i;
					break;
				}
			}
			cmbSize.setSelectedIndex(index);
		}
		txtPrice.setText(String.valueOf(itemDetail.getUnitPrice()));
		
	}
	
	private void addItemDetail() throws Exception {
		int itemId = 0;
		String name = "";
		String type = cmbType.getSelectedItem().toString();
		String size = cmbSize.getSelectedItem().toString();
		Double unitPrice = 0.0;
		
		try {
			itemId = Integer.parseInt(txtItemID.getText());
		} catch (NumberFormatException nfe) {
			String errorMessage = "You entered an invalid value for item ID: " + txtItemID.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			txtItemID.setText(null);
			txtItemID.grabFocus();
			return;
		}
		
		try {
			name = txtName.getText();
			if (name.isEmpty()) {
				String errorMessage = "You entered an invalid value for name: " + name;
				JOptionPane.showMessageDialog(null, errorMessage);
				txtName.setText(null);
				txtName.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for name: " + name;
			JOptionPane.showMessageDialog(null, errorMessage);
			txtName.setText(null);
			txtName.grabFocus();
			return;
		}
		

		try {
			unitPrice = Double.parseDouble(txtPrice.getText());
		} catch (NumberFormatException nfe) {
			String errorMessage = "You entered an invalid value for unit price: " + txtPrice.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			txtPrice.setText(null);
			txtPrice.grabFocus();
			return;
		}
		
		
		itemDetail  = new ItemDetail(false, itemId, name, type, size, unitPrice);
		
		connection.message.setItemDetail(itemDetail);
		connection.message.setOpType(connection.INSERT_ITEM_DETAIL_OP);
		connection.sendMessage();
	}

	private void modifyItemDetail() throws Exception {
		int itemId = 0;
		String name = "";
		String type = cmbType.getSelectedItem().toString();
		String size = cmbSize.getSelectedItem().toString();
		Double unitPrice = 0.0;
		
		
		try {
			itemId = Integer.parseInt(txtItemID.getText());
		} catch (NumberFormatException nfe) {
			String errorMessage = "You entered an invalid value for item ID: " + txtItemID.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			txtItemID.setText(null);
			txtItemID.grabFocus();
			return;
		}
		
		try {
			name = txtName.getText();
			if (name.isEmpty()) {
				String errorMessage = "You entered an invalid value for name: " + name;
				JOptionPane.showMessageDialog(null, errorMessage);
				txtName.setText(null);
				txtName.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for name: " + name;
			JOptionPane.showMessageDialog(null, errorMessage);
			txtName.setText(null);
			txtName.grabFocus();
			return;
		}
		

		try {
			unitPrice = Double.parseDouble(txtPrice.getText());
		} catch (NumberFormatException nfe) {
			String errorMessage = "You entered an invalid value for unit price: " + txtPrice.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			txtPrice.setText(null);
			txtPrice.grabFocus();
			return;
		}
		
		itemDetail  = new ItemDetail(false, itemId, name, type, size, unitPrice);
		
		connection.message.setItemDetail(itemDetail);
		connection.message.setOpType(connection.UPDATE_ITEM_DETAIL_OP);
		connection.sendMessage();
	}

	private void deleteItemDetail() throws Exception {
		int itemId = 0;
		
		try {
			itemId = Integer.parseInt(txtItemID.getText());
		} catch (NumberFormatException nfe) {
			String errorMessage = "You entered an invalid value for item ID: " + txtItemID.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			txtItemID.setText(null);
			txtItemID.grabFocus();
			return;
		}
		
		itemDetail  = new ItemDetail();
		itemDetail.setItemId(itemId);
		
		connection.message.setItemDetail(itemDetail);
		connection.message.setOpType(connection.DELETE_ITEM_DETAIL_OP);
		connection.sendMessage();
	}
	
	private void clearItemDetailFields() {
		txtItemID.setText(null);
		txtName.setText(null);
		cmbType.setSelectedIndex(0);
		cmbSize.setSelectedIndex(0);
		txtPrice.setText(null);
		txtItemID.grabFocus();
	}
	
}
