//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the main client login UI and functions                    *
//*                                                                                                            *
//*                                        Saved in: PizzaLogin .java                                          *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.client;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.pizza.data.User;

import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class PizzaLogin extends JPanel {
	private JTextField txtUsername;
	private JTextField txtPassword;
	private JComboBox cmbRole;
	private JButton btnLogin;
	private JButton btnRegister;
	private JButton btnExit;
	
	PizzaClient connection;

	/**
	 * Create the panel.
	 */
	public PizzaLogin(PizzaClient connection) {
		setBackground(new Color(222, 184, 135));
		this.connection = connection;
		this.initializeComponents();
		this.setActionListeners();

	}

	private void initializeComponents() {
		setLayout(null);
		
		JLabel lblUserLogin = new JLabel("User Login");
		lblUserLogin.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblUserLogin.setBounds(363, 134, 149, 40);
		add(lblUserLogin);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(267, 289, 78, 26);
		add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(267, 336, 78, 26);
		add(lblPassword);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(370, 292, 176, 23);
		add(txtUsername);
		txtUsername.setColumns(10);
		
		txtPassword = new JTextField();
		txtPassword.setBounds(370, 339, 176, 23);
		add(txtPassword);
		txtPassword.setColumns(10);
		
		cmbRole = new JComboBox();
		cmbRole.setBounds(370, 234, 176, 20);
		cmbRole.addItem(UserRole.CUSTOMER);
		cmbRole.addItem(UserRole.CHEF);
		cmbRole.addItem(UserRole.ADMIN);
		add(cmbRole);
		
		JLabel lblRole = new JLabel("Role:");
		lblRole.setBounds(267, 237, 78, 14);
		add(lblRole);
		
		btnLogin = new JButton("Login");
		btnLogin.setBounds(257, 402, 89, 21);
		add(btnLogin);
		
		btnRegister = new JButton("Register");
		btnRegister.setBounds(360, 402, 89, 21);
		add(btnRegister);

		btnExit = new JButton("Exit");
		btnExit.setBounds(472, 402, 89, 21);
		add(btnExit);
		
		JLabel lblPizzeriaPronto = new JLabel("Pizzeria Pronto");
		lblPizzeriaPronto.setFont(new Font("Vivaldi", Font.PLAIN, 60));
		lblPizzeriaPronto.setBounds(267, 43, 352, 53);
		add(lblPizzeriaPronto);

	}
	
	private void setActionListeners() {
		
		btnLogin.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					validateUser();
				} catch (Exception ex) {
					String message = "An error occurred during the user validation operation.";
					JOptionPane.showMessageDialog(null, message);
					ex.printStackTrace();
				}
			}
		});
		
		btnRegister.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				connection.changeState(ViewState.USER_REG_STATE);
			}
		});
		
		btnExit.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					connection.clientOutputStream.close();
					connection.clientInputStream.close();
				System.exit(0);
			} catch (IOException ioe) {
				String message = "An error occurred during the close operation - the system will exit.";
				JOptionPane.showMessageDialog(null, message);
				ioe.printStackTrace();
				System.exit(0);
			}
				
			}
		});
		
	}	
	
	private void validateUser() throws Exception {
		String role = cmbRole.getSelectedItem().toString();
		String username = "";
		String password = "";
		
		try {
			username = txtUsername.getText();
			if (username.isEmpty()) {
				String errorMessage = "You entered an invalid value for username: " + username;
				JOptionPane.showMessageDialog(null, errorMessage);
				txtUsername.setText(null);
				txtUsername.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for username: " + username;
			JOptionPane.showMessageDialog(null, errorMessage);
			txtUsername.setText(null);
			txtUsername.grabFocus();
			return;
		}

		try {
			password = txtPassword.getText();
			if (password.isEmpty()) {
				String errorMessage = "You entered an invalid value for password: " + password;
				JOptionPane.showMessageDialog(null, errorMessage);
				txtPassword.setText(null);
				txtPassword.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for password: " + password;
			JOptionPane.showMessageDialog(null, errorMessage);
			txtPassword.setText(null);
			txtPassword.grabFocus();
			return;
		}
		
		
		User user = new User(role, username, password);
		connection.message.setUser(user);
		connection.message.setOpType(connection.VALIDATE_USR_OP);
		connection.sendMessage();
	}
}
