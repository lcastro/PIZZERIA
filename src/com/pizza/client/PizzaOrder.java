//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the Order UI and functions                                *
//*                                                                                                            *
//*                                        Saved in: PizzaOrder.java                                           *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.client;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.pizza.data.ItemDetail;
import com.pizza.data.ItemList;
import com.pizza.data.Order;
import com.pizza.data.OrderItem;
import com.pizza.data.User;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class PizzaOrder extends JPanel {
	PizzaClient connection;
	
	private ModelMenuItem modelMenuItem;
	private ModelOrder modelOrder;
	private ModelOrderItem modelOrderItem;
	private List<ItemDetail> itemDetailList;
	private List<OrderItem> orderItemList;
	private List<ItemList> itemListList;
	private List<Order> orderList;
	
	private JTable tblMenuItems;
	private JTable tblOrderItems;
	private JTable tblOrders;
	private JScrollPane scrOrderItems;
	private JButton btnAddOrderItem;
	private JButton btnPlaceOrder;
	private JButton btnDeleteOrderItem;
	private JButton btnCancelOrder;
	private JButton btnLogout;
	private JButton btnMakePayment;
	
	
	/**
	 * Create the panel.
	 */
	public PizzaOrder(PizzaClient connection) {
		this.connection = connection;
		this.retrieveMenuItemList();
		this.selectExistingCustomerOrders();
		this.initializaComponents();
		this.setActionListeners();		
	}
	
	private void retrieveMenuItemList() {
		try {
			connection.message.setOpType(connection.SELECT_ALL_ITEM_DETAIL_OP);
			connection.sendMessage();
			this.itemDetailList = connection.message.getItemDetailList();
		} catch (Exception ex) {
			String message = "An error occurred during the selection of item details list";
			JOptionPane.showMessageDialog(null, message);
			ex.printStackTrace();
		}
	}

	private void initializaComponents() {
		setBackground(new Color(222, 184, 135));
		JScrollPane scrMenuItems = new JScrollPane();
		
		JLabel lblMenuItems = new JLabel("Menu Items");
		lblMenuItems.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JLabel lblOrder = new JLabel("Order");
		lblOrder.setFont(new Font("Tahoma", Font.PLAIN, 24));
		
		btnAddOrderItem = new JButton("Add Order Item");
		btnPlaceOrder = new JButton("Place Order");
		
		btnDeleteOrderItem = new JButton("Delete Order Item");
		
		btnCancelOrder = new JButton("Cancel Order");
		
		JLabel lblOrderItems = new JLabel("Order Items");
		lblOrderItems.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		scrOrderItems = new JScrollPane();
		
		btnLogout = new JButton("Logout");
		
		JScrollPane scrOrders = new JScrollPane();
		
		JLabel lblOrders = new JLabel("Orders");
		lblOrders.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		btnMakePayment = new JButton("Make Payment");
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(56)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
									.addComponent(lblMenuItems)
									.addGroup(groupLayout.createSequentialGroup()
										.addGap(2)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
											.addComponent(scrMenuItems, GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
											.addComponent(lblOrderItems)
											.addGroup(groupLayout.createSequentialGroup()
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
													.addGroup(groupLayout.createSequentialGroup()
														.addComponent(btnDeleteOrderItem)
														.addGap(26)
														.addComponent(btnPlaceOrder)
														.addGap(26)
														.addComponent(btnCancelOrder))
													.addComponent(scrOrderItems, GroupLayout.PREFERRED_SIZE, 414, GroupLayout.PREFERRED_SIZE))
												.addGap(8)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
													.addGroup(groupLayout.createSequentialGroup()
														.addGap(23)
														.addComponent(btnMakePayment)
														.addGap(18)
														.addComponent(btnLogout))
													.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
														.addGap(36)
														.addComponent(lblOrders)
														.addGap(184))
													.addGroup(groupLayout.createSequentialGroup()
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(scrOrders, GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE)))))))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblOrder, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
									.addGap(298))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(353)
							.addComponent(btnAddOrderItem)))
					.addContainerGap(82, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(lblOrder)
					.addGap(16)
					.addComponent(lblMenuItems, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrMenuItems, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnAddOrderItem)
					.addGap(28)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblOrderItems)
						.addComponent(lblOrders))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrOrders, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
						.addComponent(scrOrderItems, GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnDeleteOrderItem)
							.addComponent(btnPlaceOrder)
							.addComponent(btnCancelOrder))
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnMakePayment)
							.addComponent(btnLogout)))
					.addGap(21))
		);
		
		tblOrders = new JTable(new ModelOrder(connection.message.getOrderList()));
//		tblOrders = new JTable();
//		tblOrders.setModel(new DefaultTableModel(
//			new Object[][] {
//			},
//			new String[] {
//				"Select", "OrderId", "Date", "Status"
//			}
//		));
		tblOrders.getColumnModel().getColumn(0).setPreferredWidth(45);
		tblOrders.getColumnModel().getColumn(3).setPreferredWidth(147);
		scrOrders.setViewportView(tblOrders);
		
		tblOrderItems = new JTable();
		tblOrderItems.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Select", "Item", "Qty"
			}
		));
		tblOrderItems.getColumnModel().getColumn(0).setPreferredWidth(49);
		tblOrderItems.getColumnModel().getColumn(1).setPreferredWidth(253);
		scrOrderItems.setViewportView(tblOrderItems);
		
		
		
		//Menu Items Table 
		tblMenuItems = new JTable(new ModelMenuItem((this.itemDetailList)));
//		tblMenuItems = new JTable();
//		tblMenuItems.setModel(new DefaultTableModel(
//			new Object[][] {
//			},
//			new String[] {
//				"Select", "ID", "Name", "Type", "Size", "Price"
//			}
//		));
		tblMenuItems.getColumnModel().getColumn(0).setPreferredWidth(51);
		tblMenuItems.getColumnModel().getColumn(1).setPreferredWidth(64);
		tblMenuItems.getColumnModel().getColumn(2).setPreferredWidth(240);
		tblMenuItems.getColumnModel().getColumn(3).setPreferredWidth(146);
		tblMenuItems.getColumnModel().getColumn(4).setPreferredWidth(81);
		tblMenuItems.getColumnModel().getColumn(5).setPreferredWidth(110);
		scrMenuItems.setViewportView(tblMenuItems);
		setLayout(groupLayout);
	}
	
	private void setActionListeners() {
		btnAddOrderItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				orderItemList = new ArrayList<OrderItem>();
				for (int i = 0; i < itemDetailList.size(); i++) {
					ItemDetail item = itemDetailList.get(i);
					if (item.getSelection()) {
						OrderItem orderItem = new OrderItem();
						orderItem.setSelection(Boolean.FALSE);
						orderItem.setItemId(item.getItemId());
						String itemName = item.getName() + " " + item.getType() + " " + item.getSize();
						orderItem.setItem(itemName);
						orderItem.setQty(new Integer(1));
						orderItem.setUnitPrice(item.getUnitPrice());
						orderItemList.add(orderItem);
					}
				}
				tblOrderItems.setModel(new ModelOrderItem(orderItemList));
			}
		});
		
		btnPlaceOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Insert Order
				insertCustomerOrder();
				
				//Select exiting Order(s) for the customer
				selectExistingCustomerOrders();
				
				//Insert Order Items
				insertOrderItemList();
				
				//Render ModelOrder Table view
				renderOderModelTableView();
				
				resetMenuOrderItemModelTableView();				
			}
		});
		
		btnDeleteOrderItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Iterator<OrderItem> iterator = orderItemList.iterator();
				while(iterator.hasNext()) {
					OrderItem orderItem = iterator.next();
					if (orderItem.getSelection()) {
						iterator.remove();
					}
				}
				tblOrderItems.setModel(new ModelOrderItem(orderItemList));
			}
		});
	
		btnCancelOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean canCancel = false;
				for (int i = 0; i < orderList.size(); i++) {
					Order order = orderList.get(i);
					if (order.getSelection()) {
						if (order.getOrderStatus().equals(OrderStatus.RECEIVED.toString()) || 
							order.getOrderStatus().equals(OrderStatus.INPROCESS.toString())) {
							String message = "Your order can not be cancelled. It is already RECEIVED or INPROCESS";
							JOptionPane.showMessageDialog(null, message);
						} else {
							try {
								//Delete payment information
								connection.message.setOrder(order);
								connection.message.setOpType(connection.DELETE_PAYMENT_OP);
								connection.sendMessage();
								
								//Delete Order Item List
								connection.message.setOpType(connection.DELETE_ITEM_LIST_OP);
								connection.sendMessage();
								
								//Delete Order
								connection.message.setOpType(connection.DELETE_ORDER_OP);
								connection.sendMessage();
								canCancel = true;
							} catch (Exception ex) {
								String message = "An error occurred during the payment deletion operation.";
								JOptionPane.showMessageDialog(null, message);
								ex.printStackTrace();
							}
						}
					}
				}
				if (canCancel) {
					Iterator<Order> iterator = orderList.iterator();
					while(iterator.hasNext()) {
						Order order = iterator.next();
						if (order.getSelection()) {
							iterator.remove();
						}
					}
					tblOrders.setModel(new ModelOrder(orderList));
				}
			}
		});
		
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connection.changeState(ViewState.LOGIN_STATE);
			}
		});
		
		btnMakePayment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connection.changeState(ViewState.PAYMENT_STATE);
			}
		});
		
	}
	
	private void insertCustomerOrder() {
		// Calculate Order quantity and price totals
		int totalQty = 0;
		Double totalPrice = 0.0;

		for (int i = 0; i < orderItemList.size(); i++) {
			OrderItem orderItem = orderItemList.get(i);
			totalQty = totalQty + orderItem.getQty();
			totalPrice = totalPrice + orderItem.getUnitPrice();
		}
		//Insert Order
		Order order = new Order(Boolean.FALSE, totalQty, LocalDate.now(), totalPrice, OrderStatus.PLACED.toString(), connection.message.getUser());
		connection.message.setOrder(order);
		connection.message.setOpType(connection.INSERT_ORDER_OP);

		try {
			connection.sendMessage();
		} catch (Exception ex) {
			String message = "An error occurred during the insert order operation.";
			JOptionPane.showMessageDialog(null, message);
			ex.printStackTrace();
		}
	}
	
	private void selectExistingCustomerOrders() {
		try {
			connection.message.setOpType(connection.SELECT_ALL_ORDERS_OP);
			connection.sendMessage();
		} catch (Exception ex) {
			String message = "An error occurred during the selection of all orders by user operation.";
			JOptionPane.showMessageDialog(null, message);
			ex.printStackTrace();
		}
		orderList = connection.message.getOrderList();
	}
	
	private void insertOrderItemList() {
		//Prepare Item List
		itemListList = new ArrayList<ItemList>();
		for (int i = 0; i < orderList.size(); i++) {
			Order userOrder = orderList.get(i);
			int orderUserId = userOrder.getUser().getUserId();
			int loginUserId = connection.message.getUser().getUserId();
			
			if (orderUserId == loginUserId) {
				for (int j = 0; j < orderItemList.size(); j++) {
					OrderItem orderItem = orderItemList.get(j);
					ItemList itemList = new ItemList();
					itemList.setItemDetailItemId(orderItem.getItemId());
					itemList.setOrderOrderId(userOrder.getOrderId());
					itemListList.add(itemList);
				}
				
			}
		}
		// Insert Item List 
		connection.message.setItemListList(itemListList);
		connection.message.setOpType(connection.INSERT_ITEM_LIST_OP);
		try {
			connection.sendMessage();
		} catch (Exception ex) {
			String message = "An error occurred during the item list insert operation.";
			JOptionPane.showMessageDialog(null, message);
			ex.printStackTrace();
		}
	}
	
	private void renderOderModelTableView() {
		tblOrders.setModel(new ModelOrder(orderList));
	}
	
	private void resetMenuOrderItemModelTableView() {
		//Reset Menu Item List table model view 
		for (int i = 0; i < itemDetailList.size(); i++) {
			ItemDetail item = itemDetailList.get(i);
			if (item.getSelection()) {
				item.setSelection(Boolean.FALSE);
			}
		}
		tblMenuItems.setModel(new ModelMenuItem(itemDetailList));
		
		//Reset Order Item table model view
		orderItemList.clear();
		tblOrderItems.setModel(new ModelOrderItem(orderItemList));
	}
	
	
}
