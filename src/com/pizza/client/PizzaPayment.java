//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the Payment UI and functions                              *
//*                                                                                                            *
//*                                        Saved in: PizzaPayment.java                                         *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.client;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import com.pizza.data.Order;
import com.pizza.data.Payment;

import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class PizzaPayment extends JPanel {
	private JTable tblCustOrder;
	private JTextField txtOrderId;
	private JTextField txtSubTotal;
	private JTextField txtTaxes;
	private JTextField txtTotal;
	private JTextField txtCreditCardNum;
	private JTextField txtCreditCardExpDate;
	private JTextField txtCreditCardCcv;
	private JTextArea txaReceiptDisplay;
	private JButton btnPayOrder;
	private JButton btnPrintReceipt;
	private JButton btnLogout;
	private JButton btnReturn;
	
	
	PizzaClient connection;

	private List<Order> orderList;
	private final double TAX_RATE = 7.90;
	private final String PAYMENT_DESC = "Order Payment";
	private final String ORDER_PAID = "Order Payment Completed";
	private Payment payment;
	
	/**
	 * Create the panel.
	 */
	public PizzaPayment(PizzaClient connection) {
		this.connection = connection;
		this.orderList = this.connection.message.getOrderList();
		this.initializeComponents();
		this.setActionListeners();
	}
	
	private void initializeComponents() {
		setBackground(new Color(222, 184, 135));
		setLayout(null);
		
		JScrollPane scrCustOrder = new JScrollPane();
		scrCustOrder.setBounds(167, 126, 491, 70);
		add(scrCustOrder);
		
		
		tblCustOrder = new JTable();
		tblCustOrder.setModel(new ModelOrderPayment(orderList));
//		tblCustOrder.setModel(new DefaultTableModel(
//			new Object[][] {
//			},
//			new String[] {
//				"Select", "OrderId", "Date", "Quantity", "Sub-Total"
//			}
//		));
		tblCustOrder.getColumnModel().getColumn(0).setResizable(false);
		tblCustOrder.getColumnModel().getColumn(2).setPreferredWidth(90);
		tblCustOrder.getColumnModel().getColumn(4).setPreferredWidth(104);
		scrCustOrder.setViewportView(tblCustOrder);
		
		
		JLabel lblPayment = new JLabel("Payment");
		lblPayment.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblPayment.setBounds(362, 37, 103, 55);
		add(lblPayment);
		
		JLabel lblOrders = new JLabel("Orders");
		lblOrders.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblOrders.setBounds(167, 95, 46, 14);
		add(lblOrders);
		
		JLabel lblOrderid = new JLabel("OrderId:");
		lblOrderid.setBounds(274, 223, 131, 14);
		add(lblOrderid);
		
		JLabel lblSubtotal = new JLabel("Sub-Total:");
		lblSubtotal.setBounds(274, 251, 131, 14);
		add(lblSubtotal);
		
		JLabel lblTaxes = new JLabel("Taxes:");
		lblTaxes.setBounds(274, 279, 131, 14);
		add(lblTaxes);
		
		JLabel lblTotal = new JLabel("Total:");
		lblTotal.setBounds(274, 307, 131, 14);
		add(lblTotal);
		
		JLabel lblCreditCard = new JLabel("Credit Card #:");
		lblCreditCard.setBounds(274, 335, 131, 14);
		add(lblCreditCard);
		
		JLabel lblCreditCardExp = new JLabel("Credit Card Exp. Date:");
		lblCreditCardExp.setBounds(274, 363, 131, 14);
		add(lblCreditCardExp);
		
		JLabel lblCreditCardCcv = new JLabel("Credit Card CCV:");
		lblCreditCardCcv.setBounds(274, 391, 131, 14);
		add(lblCreditCardCcv);
		
		txtOrderId = new JTextField();
		txtOrderId.setEditable(false);
		txtOrderId.setBounds(443, 223, 86, 20);
		add(txtOrderId);
		txtOrderId.setColumns(10);
		
		txtSubTotal = new JTextField();
		txtSubTotal.setEditable(false);
		txtSubTotal.setBounds(443, 248, 141, 20);
		add(txtSubTotal);
		txtSubTotal.setColumns(10);
		
		txtTaxes = new JTextField();
		txtTaxes.setEditable(false);
		txtTaxes.setBounds(443, 276, 141, 20);
		add(txtTaxes);
		txtTaxes.setColumns(10);
		
		txtTotal = new JTextField();
		txtTotal.setEditable(false);
		txtTotal.setBounds(443, 304, 141, 20);
		add(txtTotal);
		txtTotal.setColumns(10);
		
		txtCreditCardNum = new JTextField();
		txtCreditCardNum.setBounds(443, 332, 141, 20);
		add(txtCreditCardNum);
		txtCreditCardNum.setColumns(10);
		
		txtCreditCardExpDate = new JTextField();
		txtCreditCardExpDate.setBounds(443, 360, 86, 20);
		add(txtCreditCardExpDate);
		txtCreditCardExpDate.setColumns(10);
		
		txtCreditCardCcv = new JTextField();
		txtCreditCardCcv.setBounds(443, 388, 86, 20);
		add(txtCreditCardCcv);
		txtCreditCardCcv.setColumns(10);
		
		btnPayOrder = new JButton("Pay Order");
		btnPayOrder.setBounds(167, 446, 118, 23);
		add(btnPayOrder);
		
		btnPrintReceipt = new JButton("Print Receipt");
		btnPrintReceipt.setBounds(295, 446, 118, 23);
		add(btnPrintReceipt);
		
		txaReceiptDisplay = new JTextArea();
		txaReceiptDisplay.setEditable(false);
		txaReceiptDisplay.setBounds(167, 503, 491, 45);
		add(txaReceiptDisplay);
		
		btnLogout = new JButton("Logout");
		btnLogout.setBounds(423, 446, 118, 23);
		add(btnLogout);
		
		btnReturn = new JButton("Return");
		btnReturn.setBounds(551, 446, 107, 23);
		add(btnReturn);

	}
	
	private void setActionListeners() {
		
	    //SET SELECTION MODE
	    tblCustOrder.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    ListSelectionModel listSelectionModel = tblCustOrder.getSelectionModel();

	    //add listener
	    listSelectionModel.addListSelectionListener(new ListSelectionListener() {

	      @Override
	      public void valueChanged(ListSelectionEvent e) {
	        TableModel modelOrderPayment = (ModelOrderPayment)tblCustOrder.getModel();
	        ListSelectionModel lsm=(ListSelectionModel) e.getSource();
	        if(!lsm.isSelectionEmpty())
	        {
	          int selectedRow=lsm.getMinSelectionIndex();
	          int orderId = (Integer)modelOrderPayment.getValueAt(selectedRow, 1);
	          for(Order orderItem : orderList){
	        	if (orderItem.getOrderId() == orderId) {
	        		//Set non-editable form data fields
	        		txtOrderId.setText(String.valueOf(orderItem.getOrderId()));
	        		txtSubTotal.setText(String.valueOf(orderItem.getPrice()));
	        		Double totTax = (orderItem.getPrice() * TAX_RATE)/100;
	        		txtTaxes.setText(String.valueOf(totTax));
	        		Double orderTot = orderItem.getPrice() + totTax;
	        		txtTotal.setText(String.valueOf(orderTot));
	        		txtCreditCardNum.grabFocus();
	        		
	        		payment = new Payment();
	        		payment.setOrder(orderItem);
	        		payment.setAmount(orderItem.getPrice());
	        		payment.setTaxes(totTax);
	        		payment.setTotal(orderTot);
	        		
	        	}
	      	  }
	        }
	      }
	    });
		
		btnPayOrder.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				LocalDate paymentDate = LocalDate.now();
				
				String creditCardNum = txtCreditCardNum.getText();
				String creditCardExpDate = txtCreditCardExpDate.getText();
				String creditCardCcv = txtCreditCardCcv.getText();
				
				try {
					Long ccNum = Long.parseLong(creditCardNum);
				} catch (NumberFormatException nfe) {
					String errorMessage = "You entered an invalid value for credit card number: " + creditCardNum;
					JOptionPane.showMessageDialog(null, errorMessage);
					txtCreditCardNum.setText(null);
					txtCreditCardNum.grabFocus();
					return;
				}

				try {
					LocalDate ccExDt = LocalDate.parse(creditCardExpDate);
				} catch(Exception ex) {
					String errorMessage = "You entered an invalid value for credit card exp date. Use format (YYYY-MM-DD) : " + creditCardExpDate;
					JOptionPane.showMessageDialog(null, errorMessage);
					txtCreditCardExpDate.setText(null);
					txtCreditCardExpDate.grabFocus();
					return;
					
				}

				try {
					Long ccCcv = Long.parseLong(creditCardCcv);
				} catch (NumberFormatException nfe) {
					String errorMessage = "You entered an invalid value for credit card CCV: " + creditCardCcv;
					JOptionPane.showMessageDialog(null, errorMessage);
					txtCreditCardCcv.setText(null);
					txtCreditCardCcv.grabFocus();
					return;
				}
				
				
				payment.setPaymentDate(paymentDate);
				payment.setDescription(PAYMENT_DESC);
				payment.setCreditCardNumber(creditCardNum);
				payment.setCreditCardCcv(creditCardCcv);
				payment.setCreditCardExpDate(LocalDate.parse(creditCardExpDate));
				
				// Insert Payment 
				connection.message.setPayment(payment);;
				connection.message.setOpType(connection.INSERT_PAYMENT_OP);
				try {
					connection.sendMessage();
				} catch (Exception ex) {
					String message = "An error occurred during the payment insert operation.";
					JOptionPane.showMessageDialog(null, message);
					ex.printStackTrace();
				}
				txaReceiptDisplay.setText(ORDER_PAID + "\n");
			}
		});
		
		btnPrintReceipt.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String customerInfo = connection.message.getUser().getName();
				List<Order> orderList = connection.message.getOrderList();
				for(Order orderItem : orderList){
					customerInfo = customerInfo + " Order #: " + orderItem.getOrderId();
				}
				txaReceiptDisplay.setText(" Customer: " + customerInfo + "\n");
			}
		});
		
		btnLogout.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				connection.changeState(ViewState.LOGIN_STATE);
			}
		});
		
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connection.changeState(ViewState.USER_ORDER_STATE);
			}
		});
	}
	
}
