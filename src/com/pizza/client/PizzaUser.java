//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the User UI and functions                                 *
//*                                                                                                            *
//*                                        Saved in: PizzaUser.java                                            *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.client;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import com.pizza.data.User;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class PizzaUser extends JPanel {
	PizzaClient connection;
	
	private JTextField txtUsername;
	private JTextField txtPassword;
	private JTextField txtName;
	private JTextField txtMailAddress;
	private JTextField txtEmailAddress;
	private JTextField txtPhoneNumber;
	private JButton btnAddUser;
	private JButton btnCancel;
	private JComboBox cmbRole; 
	private User user;
	
	
	/**
	 * Create the panel.
	 */
	public PizzaUser(PizzaClient connection) {
		setBackground(new Color(222, 184, 135));
		this.connection = connection;
		this.initializeComponents();
		this.setActionListeners();
	}
	
	private void initializeComponents() {
		setLayout(null);
		JLabel lblUserRegistration = new JLabel("User Registration");
		lblUserRegistration.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblUserRegistration.setBounds(296, 75, 242, 46);
		add(lblUserRegistration);
		
		JLabel lblRole = new JLabel("Role:");
		lblRole.setBounds(215, 167, 104, 14);
		add(lblRole);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(215, 215, 104, 14);
		add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(215, 254, 104, 14);
		add(lblPassword);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(215, 299, 104, 14);
		add(lblName);
		
		JLabel lblMailAddress = new JLabel("Mail Address:");
		lblMailAddress.setBounds(215, 345, 104, 14);
		add(lblMailAddress);
		
		JLabel lblEmailAddress = new JLabel("Email Address:");
		lblEmailAddress.setBounds(214, 388, 105, 14);
		add(lblEmailAddress);
		
		JLabel lblPhoneNumber = new JLabel("Phone Number:");
		lblPhoneNumber.setBounds(215, 431, 104, 14);
		add(lblPhoneNumber);
		
		cmbRole = new JComboBox();
		cmbRole.setBounds(355, 167, 105, 20);
		cmbRole.addItem(UserRole.CUSTOMER);
		cmbRole.addItem(UserRole.ADMIN);
		cmbRole.addItem(UserRole.CHEF);
		add(cmbRole);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(352, 215, 172, 20);
		add(txtUsername);
		txtUsername.setColumns(10);
		
		txtPassword = new JTextField();
		txtPassword.setBounds(352, 254, 172, 20);
		add(txtPassword);
		txtPassword.setColumns(10);
		
		txtName = new JTextField();
		txtName.setBounds(352, 299, 280, 20);
		add(txtName);
		txtName.setColumns(10);
		
		txtMailAddress = new JTextField();
		txtMailAddress.setBounds(352, 345, 277, 20);
		add(txtMailAddress);
		txtMailAddress.setColumns(10);
		
		txtEmailAddress = new JTextField();
		txtEmailAddress.setBounds(352, 388, 277, 20);
		add(txtEmailAddress);
		txtEmailAddress.setColumns(10);
		
		txtPhoneNumber = new JTextField();
		txtPhoneNumber.setBounds(352, 431, 172, 20);
		add(txtPhoneNumber);
		txtPhoneNumber.setColumns(10);
		
		btnAddUser = new JButton("Add User");
		btnAddUser.setBounds(230, 477, 89, 23);
		add(btnAddUser);

		btnCancel = new JButton("Cancel");
		btnCancel.setBounds(435, 477, 89, 23);
		add(btnCancel);
		
	}
	
	private void setActionListeners() {
		btnAddUser.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					addUser();
				} catch (Exception ex) {
					String message = "An error occurred during the insert user operation.";
					JOptionPane.showMessageDialog(null, message);
					ex.printStackTrace();
				}
			}
			
		});
		
		btnCancel.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				connection.changeState(ViewState.LOGIN_STATE);
				
			}
		});
		
	}
	
	private void addUser() throws Exception {
		String role = cmbRole.getSelectedItem().toString();
		
		String username = "";
		String password = "";
		String name = "";
		String mailAddress = "";
		String emailAddress = "";
		String phoneNumber = "";
		
		
		try {
			username = txtUsername.getText();
			if (username.isEmpty()) {
				String errorMessage = "You entered an invalid value for username: " + username;
				JOptionPane.showMessageDialog(null, errorMessage);
				txtUsername.setText(null);
				txtUsername.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for username: " + username;
			JOptionPane.showMessageDialog(null, errorMessage);
			txtUsername.setText(null);
			txtUsername.grabFocus();
			return;
		}

		try {
			password = txtPassword.getText();
			if (password.isEmpty()) {
				String errorMessage = "You entered an invalid value for password: " + password;
				JOptionPane.showMessageDialog(null, errorMessage);
				txtPassword.setText(null);
				txtPassword.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for password: " + password;
			JOptionPane.showMessageDialog(null, errorMessage);
			txtPassword.setText(null);
			txtPassword.grabFocus();
			return;
		}

		try {
			name = txtName.getText();
			if (name.isEmpty()) {
				String errorMessage = "You entered an invalid value for name: " + name;
				JOptionPane.showMessageDialog(null, errorMessage);
				txtName.setText(null);
				txtName.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for name: " + name;
			JOptionPane.showMessageDialog(null, errorMessage);
			txtName.setText(null);
			txtName.grabFocus();
			return;
		}

		try {
			mailAddress = txtMailAddress.getText();
			if (mailAddress.isEmpty()) {
				String errorMessage = "You entered an invalid value for mail address: " + mailAddress;
				JOptionPane.showMessageDialog(null, errorMessage);
				txtMailAddress.setText(null);
				txtMailAddress.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for mail address: " + mailAddress;
			JOptionPane.showMessageDialog(null, errorMessage);
			txtMailAddress.setText(null);
			txtMailAddress.grabFocus();
			return;
		}

		try {
			emailAddress = txtEmailAddress.getText();
			if (emailAddress.isEmpty()) {
				String errorMessage = "You entered an invalid value for email address: " + emailAddress;
				JOptionPane.showMessageDialog(null, errorMessage);
				txtEmailAddress.setText(null);
				txtEmailAddress.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for email address: " + emailAddress;
			JOptionPane.showMessageDialog(null, errorMessage);
			txtEmailAddress.setText(null);
			txtEmailAddress.grabFocus();
			return;
		}

		try {
			phoneNumber = txtPhoneNumber.getText();
			if (phoneNumber.isEmpty()) {
				String errorMessage = "You entered an invalid value for phone number: " + phoneNumber;
				JOptionPane.showMessageDialog(null, errorMessage);
				txtPhoneNumber.setText(null);
				txtPhoneNumber.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for phone number: " + phoneNumber;
			JOptionPane.showMessageDialog(null, errorMessage);
			txtPhoneNumber.setText(null);
			txtPhoneNumber.grabFocus();
			return;
		}
		
		User user = new User(role, username, password, name, mailAddress, emailAddress, phoneNumber);
		connection.message.setUser(user);
		connection.message.setOpType(connection.INSERT_USR_OP);
		connection.sendMessage();
	}
	
}
