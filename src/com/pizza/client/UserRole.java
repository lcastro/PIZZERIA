//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the User roles as an ENUM type                            *
//*                                                                                                            *
//*                                        Saved in: UserRole.java                                             *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.client;

public enum UserRole {
	CUSTOMER,
	CHEF,
	ADMIN
}
