//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the State Management of the UI rendering                  *
//*                            based on user type and function based                                           *
//*                                                                                                            *
//*                                        Saved in: ViewState.java                                            *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.client;

public enum ViewState {
	LOGIN_STATE,
	USER_REG_STATE,
	USER_ORDER_STATE,
	ADMIN_STATE,
	CHEF_STATE,
	PAYMENT_STATE
}
