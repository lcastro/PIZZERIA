//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the DTO for the ItemDetail entity                         *
//*                                                                                                            *
//*                                        Saved in: ItemDetail.java                                           *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.data;

import java.io.Serializable;

public class ItemDetail implements Serializable {
	private final long serialVersionUID = 2636390384284318161L;
	
	private Boolean selection;
	private int itemId;
	private String name;
	private String type;
	private String size;
	private double unitPrice;
	
	public ItemDetail() {}
	
	public ItemDetail(Boolean selection, int itemId, String name, String type, String size, double unitPrice) {
		this.selection = selection;
		this.itemId = itemId;
		this.name = name;
		this.type = type;
		this.size = size;
		this.unitPrice = unitPrice;
	}

	public Boolean getSelection() {
		return selection;
	}
	public void setSelection(Boolean selection) {
		this.selection = selection;
	}
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
}
