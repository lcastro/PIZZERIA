//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the DTO for the ItemList entity                           *
//*                                                                                                            *
//*                                        Saved in: ItemList.java                                             *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.data;

import java.io.Serializable;

public class ItemList implements Serializable {
	private final long serialVersionUID = 2636390384284318160L;
	
	private int itemListId;
	private int itemDetailItemId;
	private int orderOrderId;
	
	public ItemList() {}
	
	public ItemList(int itemDetailItemId, int orderOderId) {
		this.itemDetailItemId = itemDetailItemId;
		this.orderOrderId = orderOrderId;
		
	}
	public int getItemListId() {
		return itemListId;
	}
	public void setItemListId(int itemListId) {
		this.itemListId = itemListId;
	}
	public int getItemDetailItemId() {
		return itemDetailItemId;
	}
	public void setItemDetailItemId(int itemDetailItemId) {
		this.itemDetailItemId = itemDetailItemId;
	}
	public int getOrderOrderId() {
		return orderOrderId;
	}
	public void setOrderOrderId(int orderOrderId) {
		this.orderOrderId = orderOrderId;
	}

}
