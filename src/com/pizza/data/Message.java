//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the data message communication between                    *
//*                            client and server applications                                                  *
//*                                                                                                            *
//*                                        Saved in: Message.java                                              *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.data;

import java.io.Serializable;
import java.util.List;

public class Message implements Serializable {
	private final long serialVersionUID = 2636390384284318165L;

	private User user;
	private Order order;
	private Payment payment;
	private ItemList itemList;
	private ItemDetail itemDetail;
	
	private List<Order> orderList;
	private List<Order> updateOrderList;
	private List<ItemList> itemListList;
	private List<ItemDetail> itemDetailList;
	private String orderStatus;
	
	private int opType;
	private String opStatus;
	private String errorMsg;
	
	
	public Message() {
		
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public ItemList getItemList() {
		return itemList;
	}

	public void setItemList(ItemList itemList) {
		this.itemList = itemList;
	}

	public ItemDetail getItemDetail() {
		return itemDetail;
	}

	public void setItemDetail(ItemDetail itemDetail) {
		this.itemDetail = itemDetail;
	}

	public List<Order> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}

	public List<Order> getUpdateOrderList() {
		return updateOrderList;
	}

	public void setUpdateOrderList(List<Order> updateOrderList) {
		this.updateOrderList = updateOrderList;
	}
	
	public List<ItemList> getItemListList() {
		return itemListList;
	}

	public void setItemListList(List<ItemList> itemListList) {
		this.itemListList = itemListList;
	}

	public List<ItemDetail> getItemDetailList() {
		return itemDetailList;
	}

	public void setItemDetaiList(List<ItemDetail> itemDetailList) {
		this.itemDetailList = itemDetailList;
	}
	
	public int getOpType() {
		return opType;
	}

	public void setOpType(int opType) {
		this.opType = opType;
	}

	public String getOpStatus() {
		return opStatus;
	}

	public void setOpStatus(String opStatus) {
		this.opStatus = opStatus;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

}

