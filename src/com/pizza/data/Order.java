//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the DTO for the Order entity                              *
//*                                                                                                            *
//*                                        Saved in: Order.java                                                *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public class Order implements Serializable {
	private final long serialVersionUID = 2636390384284318164L;
	
	private Boolean selection;
	private int orderId;
	private int quantity;
	private LocalDate orderDate;
	private double price;
	private String orderStatus;
	private User user;
	private List<ItemList> itemList;
	
	public Order() {
	}
	
	public Order(Boolean selection, int quantity, LocalDate orderDate, double price, String orderStatus, User user) {
		this.selection = selection;
		this.quantity = quantity;
		this.orderDate = orderDate;
		this.price = price;
		this.orderStatus = orderStatus;
		this.user = user;
	}
	
	public Boolean getSelection() {
		return selection;
	}
	public void setSelection(Boolean selection) {
		this.selection = selection;
	}
	
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public LocalDate getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<ItemList> getItemList() {
		return itemList;
	}
	public void setItemList(List<ItemList> itemList) {
		this.itemList = itemList;
	}
}
