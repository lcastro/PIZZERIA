//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the DTO for the OrderItem entity                          *
//*                                                                                                            *
//*                                        Saved in: OrderItem.java                                            *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.data;

import java.io.Serializable;

public class OrderItem implements Serializable {
	private final long serialVersionUID = 2636390384284318161L;
	
	private Boolean selection;
	private int itemId;
	private String item;
	private Integer qty;
	private Double unitPrice;
	
	public OrderItem() {}
	
	public OrderItem(Boolean selection, int itemId, String item, Integer qty, Double unitPrice) {
		this.selection = selection;
		this.itemId = itemId;
		this.item = item;
		this.qty = qty;
		this.unitPrice = unitPrice;
	}

	public Boolean getSelection() {
		return selection;
	}
	public void setSelection(Boolean selection) {
		this.selection = selection;
	}
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
}
