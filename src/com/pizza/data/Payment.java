//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the DTO for the Payment entity                            *
//*                                                                                                            *
//*                                        Saved in: Payment.java                                              *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.data;

import java.io.Serializable;
import java.time.LocalDate;

public class Payment implements Serializable {
	private final long serialVersionUID = 2636390384284318168L;

	private int paymentId;
	private LocalDate paymentDate;
	private String description;
	private double amount;
	private String creditCardNumber;
	private String creditCardCcv;
	private LocalDate creditCardExpDate;
	private double taxes;
	private double total;
	private Order order;
	
	public Payment() {
	}
	
	public Payment(int paymentId, LocalDate paymentDate, String description, double amount, String creditCardNumber, String creditCardCcv, LocalDate creditCardExpDate, double taxes, double total, Order order) {
		this.paymentId = paymentId;
		this.paymentDate = paymentDate;
		this.description = description;
		this.amount = amount;
		this.creditCardNumber = creditCardNumber;
		this.creditCardCcv = creditCardCcv;
		this.creditCardExpDate = creditCardExpDate;
		this.taxes = taxes;
		this.total = total;
		this.order = order;
	}
	
	public int getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	public LocalDate getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public String getCreditCardCcv() {
		return creditCardCcv;
	}
	public void setCreditCardCcv(String creditCardCcv) {
		this.creditCardCcv = creditCardCcv;
	}
	public LocalDate getCreditCardExpDate() {
		return creditCardExpDate;
	}
	public void setCreditCardExpDate(LocalDate creditCardExpDate) {
		this.creditCardExpDate = creditCardExpDate;
	}
	public double getTaxes() {
		return taxes;
	}
	public void setTaxes(double taxes) {
		this.taxes = taxes;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	
}
