//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the DTO for the User entity                               *
//*                                                                                                            *
//*                                        Saved in: User.java                                                 *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.data;

import java.io.Serializable;

public class User implements Serializable {
	
	private final long serialVersionUID = 2636390384284318163L;
	
	private int userId;
	private String role;
	private String userName;
	private String password;
	private String name;
	private String mailAddress;
	private String emailAddress;
	private String phoneNumber;
	
	public User() {
	}

	public User(String role, String userName, String password) {
		this.role = role;
		this.userName = userName;
		this.password = password;
	}
	
	public User(String role, String userName, String password, String name, String mailAddress, String emailAddress, String phoneNumber) {
		this.role = role;
		this.userName = userName;
		this.password = password;
		this.name = name;
		this.mailAddress = mailAddress;
		this.emailAddress = emailAddress;
		this.phoneNumber = phoneNumber;
	}
	
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMailAddress() {
		return mailAddress;
	}
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
}
