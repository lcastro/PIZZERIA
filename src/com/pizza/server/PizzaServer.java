//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represents the main class for the server application                 *
//*                                                                                                            *
//*                                        Saved in: PizzaServer.java                                          *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.server;

import java.awt.*;
import java.net.Socket;
import javax.swing.*;

import com.pizza.data.Message;
import com.pizza.server.sql.ItemDetailSql;
import com.pizza.server.sql.ItemListSql;
import com.pizza.server.sql.OrderSql;
import com.pizza.server.sql.PaymentSql;
import com.pizza.server.sql.UserSql;

import java.io.*;
import java.net.*;
import java.sql.*;

public class PizzaServer {

	private PizzaServerGui gui;
	private int port;
	private Message message;
	private Socket socket;
	private ObjectInputStream serverInputStream;
	private ObjectOutputStream serverOutputStream; 
	private Connection connection;
	
	private UserSql userSql;
	private OrderSql orderSql;
	private PaymentSql paymentSql;
	private ItemListSql itemListSql;
	private ItemDetailSql itemDetailSql;
	
	
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://BUSCISMYSQL01:3306/locastrodb";
	static final String USER = "locastro";
	static final String PASS = "c618a!04574";
	
	private final int RETRIEVE_USR_OP = 100;
	private final int INSERT_USR_OP = 110;
	private final int UPDATE_USR_OP = 120;
	private final int DELETE_USR_OP = 130;
	private final int VALIDATE_USR_OP = 140;
	
	private final int VIEW_ORDER_OP = 200;
	private final int INSERT_ORDER_OP = 210;
	private final int UPDATE_ORDER_OP = 220;
	private final int DELETE_ORDER_OP = 230;
	private final int SELECT_ALL_ORDERS_OP = 240;
	private final int SELECT_ORDERS_OP = 250;
	private final int UPDATE_ORDER_STATUS_OP = 260;

	private final int VIEW_PAYMENT_OP = 300;
	private final int INSERT_PAYMENT_OP = 310;
	private final int UPDATE_PAYMENT_OP = 320;
	private final int DELETE_PAYMENT_OP = 330;

	private final int VIEW_ITEM_LIST_OP = 400;
	private final int INSERT_ITEM_LIST_OP = 410;
	private final int UPDATE_ITEM_LIST_OP = 420;
	private final int DELETE_ITEM_LIST_OP = 430;
	private final int RETRIEVE_ITEM_LIST_OP = 440;

	private final int VIEW_ITEM_DETAIL_OP = 500;
	private final int INSERT_ITEM_DETAIL_OP = 510;
	private final int UPDATE_ITEM_DETAIL_OP = 520;
	private final int DELETE_ITEM_DETAIL_OP = 530;
	private final int SELECT_ALL_ITEM_DETAIL_OP = 540;

	
	private final String SUCCESS = "S";
	private final String FAILURE = "F";
	
	public PizzaServer(int port) {
		this.port = port;
		viewGui();
	}

	private void initializeDB() throws IOException, SQLException, ClassNotFoundException {
		// create the server
		ServerSocket serverSocket = new ServerSocket(port);
		gui.txtDisplayResults.append("Server Is Listening ON Port:  " + port + "\n");
		// Connect to your database using your credentials
		// Load the JDBC driver
		Class.forName(JDBC_DRIVER);
		// Establish a connection
		connection = DriverManager.getConnection(DB_URL, USER, PASS);
		gui.txtDisplayResults.append("MySQL database connection established" + "\n");
		// loops for ever waiting for the client connection requests
		// create a thread for each client connection request using Runnable
		// class HandleAClient
		
		userSql = new UserSql(connection);
		orderSql = new OrderSql(connection);
		paymentSql = new PaymentSql(connection);
		itemListSql = new ItemListSql(connection);
		itemDetailSql = new ItemDetailSql(connection);
		
		
		while (true) {
			// Listen for a new connection request
			socket = serverSocket.accept();
			gui.txtDisplayResults.append("Connection Request ....... Connection Accepted" + "\n");

			// Create a new thread for the connection
			HandleAClient task = new HandleAClient(socket);

			// Start the new thread
			new Thread(task).start();
		}

	}

	// inner Runnable class handle a client connection
	class HandleAClient implements Runnable {
		private Socket socket; // A connected socket

		/** Construct a thread */
		public HandleAClient(Socket socket) {
			this.socket = socket;
		}

		/** Run a thread */
		public void run() {
			try {
				serverInputStream = new ObjectInputStream(socket.getInputStream());
				serverOutputStream = new ObjectOutputStream(socket.getOutputStream());
				// Continuously serve the client
				while (true) {
					message = (Message) serverInputStream.readObject();
					switch (message.getOpType()) {
					case RETRIEVE_USR_OP:
						userSql.setParams(message);
						userSql.retrieveUser();
						gui.txtDisplayResults.append("Retrieve User");
						break;
					case INSERT_USR_OP:
						userSql.setParams(message);
						userSql.insertUser();
						gui.txtDisplayResults.append("Insert User");
						break;
					case VALIDATE_USR_OP:
						userSql.setParams(message);
						userSql.validateLoginCredentials();
						gui.txtDisplayResults.append("User Validated");
						break;
					case VIEW_ORDER_OP:
						orderSql.setParams(message);
						orderSql.viewOrder();
						break;
					case INSERT_ORDER_OP:
						orderSql.setParams(message);
						orderSql.insertOrder();
						break;
					case UPDATE_ORDER_OP:
						orderSql.setParams(message);
						orderSql.updateOrder();
						break;
					case DELETE_ORDER_OP:
						orderSql.setParams(message);
						orderSql.deleteOrder();
						break;
					case SELECT_ALL_ORDERS_OP:
						orderSql.setParams(message);
						orderSql.getOrdersByUserId();
						break;
					case SELECT_ORDERS_OP:
						orderSql.setParams(message);
						orderSql.getAllOrders();
						break;
					case UPDATE_ORDER_STATUS_OP:
						orderSql.setParams(message);
						orderSql.updateOrderStatus();
						break;
					case VIEW_PAYMENT_OP:
						paymentSql.setParams(message);
						paymentSql.viewPayment();
						break;
					case INSERT_PAYMENT_OP:
						paymentSql.setParams(message);
						paymentSql.insertPayment();
						break;
					case UPDATE_PAYMENT_OP:
						paymentSql.setParams(message);
						paymentSql.updatePayment();
						break;
					case DELETE_PAYMENT_OP:
						paymentSql.setParams(message);
						paymentSql.deletePayment();
						break;
					case VIEW_ITEM_LIST_OP:
						itemListSql.viewItemList();
						break;
					case INSERT_ITEM_LIST_OP:
						itemListSql.setParams(message);
						itemListSql.insertItemList();
						break;
					case UPDATE_ITEM_LIST_OP:
						itemListSql.setParams(message);
						itemListSql.updateItemList();
						break;
					case DELETE_ITEM_LIST_OP:
						itemListSql.setParams(message);
						itemListSql.deleteItemList();
						break;
					case RETRIEVE_ITEM_LIST_OP:
						itemListSql.setParams(message);
						itemListSql.retrieveItemListByOrderId();
						break;
					case VIEW_ITEM_DETAIL_OP:
						itemDetailSql.setParams(message);
						itemDetailSql.viewItemDetail();
						gui.txtDisplayResults.append("Item Detail Viewed");
						break;
					case INSERT_ITEM_DETAIL_OP:
						itemDetailSql.setParams(message);
						itemDetailSql.insertItemDetail();
						gui.txtDisplayResults.append("Item Detail Inserted");
						break;
					case UPDATE_ITEM_DETAIL_OP:
						itemDetailSql.setParams(message);
						itemDetailSql.updateItemDetail();
						gui.txtDisplayResults.append("Item Detail Updated");
						break;
					case DELETE_ITEM_DETAIL_OP:
						itemDetailSql.setParams(message);
						itemDetailSql.deleteItemDetail();
						gui.txtDisplayResults.append("Item Detail Deleted");
						break;
					case SELECT_ALL_ITEM_DETAIL_OP:
						itemDetailSql.setParams(message);
						itemDetailSql.getAllItemDetail();
						gui.txtDisplayResults.append("Item Detail List Selected");
						break;
					}
					
					serverOutputStream.writeObject(message);
				}

			} catch (Exception ex) {
				String message = "The system will exit.";
				JOptionPane.showMessageDialog(null, message);
				ex.printStackTrace();
				System.exit(0);
			}

		}
	}// end of class Runnable

	void viewGui() {

		JFrame frame = new JFrame();
		frame.setTitle("Pizza Server");
		Container contentPane = frame.getContentPane();
		gui = new PizzaServerGui(this);
		contentPane.add(gui);
		frame.pack();
		frame.setVisible(true);

	}
	
	public static void main(String[] args) {
		PizzaServer server = new PizzaServer(8001);
		try {
			server.initializeDB();
		} catch(Exception e) {
			String message = "An error occurred in the Pizza Server application - the system will exit.";
			JOptionPane.showMessageDialog(null, message);
			e.printStackTrace();
			System.exit(0);
		}

	}
}
