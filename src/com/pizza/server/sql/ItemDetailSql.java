//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represent the SQL queries for the ItemDetail entity                  *
//*                                                                                                            *
//*                                        Saved in: ItemDetailSql.java                                        *
//*                                                                                                            *
//**************************************************************************************************************

package com.pizza.server.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.pizza.data.ItemDetail;
import com.pizza.data.ItemList;
import com.pizza.data.Message;
import com.pizza.data.User;

public class ItemDetailSql {
	private Statement statement;
	private ResultSet resultSet;
	private PreparedStatement preparedStatement;
	private Message message;
	private Connection connection;
	private ItemDetail itemDetail;
	private List<ItemDetail> itemDetailList;

	private final String SUCCESS = "S";
	private final String FAILURE = "F";

	private final int VIEW_ITEM_DETAIL_OP = 500;
	private final int INSERT_ITEM_DETAIL_OP = 510;
	private final int UPDATE_ITEM_DETAIL_OP = 520;
	private final int DELETE_ITEM_DETAIL_OP = 530;
	private final int SELECT_ALL_ITEM_DETAIL_OP = 540;

	public ItemDetailSql(Connection connection) {
		this.connection = connection;
	}

	public void setParams(Message message) {
		this.message = message;
		if (message.getItemDetail() != null) {
			this.itemDetail = message.getItemDetail();
		}
		if (message.getItemDetailList() != null) {
			this.itemDetailList = message.getItemDetailList();
		} else {
			this.itemDetailList = new ArrayList<ItemDetail>();
		}
	}
	
	public void insertItemDetail() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from itemdetail where item_id = " + itemDetail.getItemId() + ";");

		if (!resultSet.next()) {
			preparedStatement = connection.prepareStatement("insert into  itemdetail values (?, ?, ?, ?, ?)");
			preparedStatement.setInt(1, itemDetail.getItemId());
			preparedStatement.setString(2, itemDetail.getName());
			preparedStatement.setString(3, itemDetail.getType());
			preparedStatement.setString(4, itemDetail.getSize());
			preparedStatement.setDouble(5, itemDetail.getUnitPrice());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID already exists in the database");
		}
		message.setOpType(INSERT_ITEM_DETAIL_OP);
	}

	public void viewItemDetail() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from itemdetail where item_id = " + itemDetail.getItemId() + ";");

		if (resultSet.next()) {
			itemDetail.setItemId(resultSet.getInt(1));
			itemDetail.setName(resultSet.getString(2));
			itemDetail.setType(resultSet.getString(3));
			itemDetail.setSize(resultSet.getString(4));
			itemDetail.setUnitPrice(resultSet.getDouble(5));
			message.setItemDetail(itemDetail);
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(VIEW_ITEM_DETAIL_OP);
	}

	public void updateItemDetail() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from itemdetail where item_id= " + itemDetail.getItemId() + ";");

		if (resultSet.next()) {
			preparedStatement = connection.prepareStatement(
					"update itemdetail set name = ?, type = ?, size = ?, unit_price = ? WHERE item_id = ?");
			preparedStatement.setString(1, itemDetail.getName());
			preparedStatement.setString(2, itemDetail.getType());
			preparedStatement.setString(3, itemDetail.getSize());
			preparedStatement.setDouble(4, itemDetail.getUnitPrice());
			preparedStatement.setInt(5, itemDetail.getItemId());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(UPDATE_ITEM_DETAIL_OP);
	}

	public void deleteItemDetail() throws SQLException {

		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from itemdetail where item_id = " + itemDetail.getItemId() + ";");

		if (resultSet.next()) {
			preparedStatement = connection.prepareStatement("delete from itemdetail where item_id = ?");
			preparedStatement.setLong(1, itemDetail.getItemId());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(DELETE_ITEM_DETAIL_OP);
	}
	
	public void getAllItemDetail() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select item_id, name, type, size, unit_price from itemdetail;");

		while(resultSet.next()) {
			ItemDetail item = new ItemDetail();
			item.setSelection(Boolean.FALSE);
			item.setItemId(resultSet.getInt(1));
			item.setName(resultSet.getString(2));
			item.setType(resultSet.getString(3));
			item.setSize(resultSet.getString(4));
			item.setUnitPrice(resultSet.getDouble(5));
			itemDetailList.add(item);
		} 
		message.setItemDetaiList(itemDetailList);
		message.setOpStatus(SUCCESS);
		message.setOpType(SELECT_ALL_ITEM_DETAIL_OP);
	}

	
}
