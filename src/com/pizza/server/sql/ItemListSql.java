//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represent the SQL queries for the ItemList entity                    *
//*                                                                                                            *
//*                                        Saved in: ItemListSql.java                                          *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.server.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.pizza.data.ItemList;
import com.pizza.data.Message;
import com.pizza.data.Order;

public class ItemListSql {
	private Statement statement;
	private ResultSet resultSet;
	private PreparedStatement preparedStatement;
	private Message message;
	private Connection connection;
	private ItemList itemList;
	private List<ItemList> itemListList;
	private Order order;

	private final String SUCCESS = "S";
	private final String FAILURE = "F";

	private final int VIEW_ITEM_LIST_OP = 400;
	private final int INSERT_ITEM_LIST_OP = 410;
	private final int UPDATE_ITEM_LIST_OP = 420;
	private final int DELETE_ITEM_LIST_OP = 430;
	private final int RETRIEVE_ITEM_LIST_OP = 440;

	public ItemListSql(Connection connection) {
		this.connection = connection;
	}

	public void setParams(Message message) {
		this.message = message;
		
		if (message.getOrder() != null) {
			this.order = message.getOrder();
		}
		if (message.getOpType() == INSERT_ITEM_LIST_OP) {
			itemListList = message.getItemListList();
		}
		
		if (message.getOpType() == RETRIEVE_ITEM_LIST_OP) {
			itemListList = new ArrayList<ItemList>();
		}
		
	}
	
	public void insertItemList() throws SQLException {
		for (int i = 0; i < itemListList.size(); i++) {
			ItemList itemList = itemListList.get(i);
			// statements allow to issue SQL queries to the database
			statement = connection.createStatement();
			// resultSet gets the result of the SQL query
			resultSet = statement.executeQuery("select item_list_id from itemlist where item_list_id = " + itemList.getItemListId() + ";");

			if (!resultSet.next()) {
				preparedStatement = connection.prepareStatement("insert into itemlist (ItemDetail_item_id, Order_order_id) values (?, ?)");
				preparedStatement.setInt(1, itemList.getItemDetailItemId());
				preparedStatement.setInt(2, itemList.getOrderOrderId());
				preparedStatement.executeUpdate();
				message.setOpStatus(SUCCESS);
			} else {
				message.setOpStatus(FAILURE);
				message.setErrorMsg("ID already exists in the database");
			}
		}
		message.setOpType(INSERT_ITEM_LIST_OP);
	}

	public void viewItemList() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from ItemList where item_list_id = " + itemList.getItemListId() + ";");

		if (resultSet.next()) {
			itemList.setItemListId(resultSet.getInt(1));
			itemList.setItemDetailItemId(resultSet.getInt(2));
			itemList.setOrderOrderId(resultSet.getInt(3));
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(VIEW_ITEM_LIST_OP);
	}

	public void updateItemList() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from ItemList Where item_list_id= " + itemList.getItemListId() + ";");

		if (resultSet.next()) {
			preparedStatement = connection.prepareStatement(
					"update ItemList SET ItemDetail_item_id = ?, Order_order_id = ? WHERE item_list_id = ?");
			preparedStatement.setInt(1, itemList.getItemDetailItemId());
			preparedStatement.setInt(2, itemList.getOrderOrderId());
			preparedStatement.setInt(3, itemList.getItemListId());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(UPDATE_ITEM_LIST_OP);
	}

	public void deleteItemList() throws SQLException {

		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from ItemList where Order_order_id = " + order.getOrderId() + ";");

		if (resultSet.next()) {
			preparedStatement = connection.prepareStatement("delete from itemlist where Order_order_id = ?");
			preparedStatement.setInt(1, order.getOrderId());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(DELETE_ITEM_LIST_OP);
	}

	public void retrieveItemListByOrderId() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select item_list_id, ItemDetail_item_id, Order_order_id from itemlist where Order_order_id = " + order.getOrderId() + ";");

		while (resultSet.next()) {
			itemList = new ItemList();
			itemList.setItemListId(resultSet.getInt(1));
			itemList.setItemDetailItemId(resultSet.getInt(2));
			itemList.setOrderOrderId(resultSet.getInt(3));
			itemListList.add(itemList);
		}
		message.setItemListList(itemListList);
		message.setOpStatus(SUCCESS);
		message.setOpType(RETRIEVE_ITEM_LIST_OP);
	}
	
}
