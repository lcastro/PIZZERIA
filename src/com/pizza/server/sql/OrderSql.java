//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represent the SQL queries for the Order entity                       *
//*                                                                                                            *
//*                                        Saved in: OrderSql.java                                             *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.server.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.pizza.data.ItemDetail;
import com.pizza.data.Message;
import com.pizza.data.Order;
import com.pizza.data.User;

public class OrderSql {
	private Statement statement;
	private ResultSet resultSet;
	private PreparedStatement preparedStatement;
	private Message message;
	private Connection connection;
	private Order order;
	private User user;
	private Date orderDate;
	private List<Order> orderList;
	private List<Order> updateOrderList;
	private String orderStatus;


	private final String SUCCESS = "S";
	private final String FAILURE = "F";

	private final int VIEW_ORDER_OP = 200;
	private final int INSERT_ORDER_OP = 210;
	private final int UPDATE_ORDER_OP = 220;
	private final int DELETE_ORDER_OP = 230;
	private final int SELECT_ALL_ORDERS_OP = 240;
	private final int SELECT_ORDERS_OP = 250;
	private final int UPDATE_ORDER_STATUS_OP = 260;

	public OrderSql(Connection connection) {
		this.connection = connection;
	}

	public void setParams(Message message) {
		this.message = message;
		if (message.getOrder() != null) {
			this.order = message.getOrder();
			orderDate = Date.valueOf(order.getOrderDate());
		}
		this.orderList = new ArrayList<Order>();
		
		if (message.getOpType() == (UPDATE_ORDER_STATUS_OP)) {
			this.orderStatus = message.getOrderStatus();
		}
	}
	
	public void insertOrder() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		String query = "select * from locastrodb.order where order_id = " + order.getOrderId() + ";";
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery(query);

		if (!resultSet.next()) {
			preparedStatement = connection.prepareStatement("insert into locastrodb.order (quantity, date, price, status, User_user_id) values (?, ?, ?, ?, ?)");
			preparedStatement.setInt(1, order.getQuantity());
			preparedStatement.setDate(2, orderDate);
			preparedStatement.setDouble(3, order.getPrice());
			preparedStatement.setString(4, order.getOrderStatus());
			preparedStatement.setInt(5, order.getUser().getUserId());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID already exists in the database");
		}
		message.setOpType(INSERT_ORDER_OP);
	}

	public void viewOrder() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from locastrodb.order where order_id = " + order.getOrderId() + ";");

		if (resultSet.next()) {
			order.setOrderId(resultSet.getInt(1));
			order.setQuantity(resultSet.getInt(2));
			order.setOrderDate(resultSet.getDate(3).toLocalDate());
			order.setPrice(resultSet.getDouble(4));
			order.setOrderStatus(resultSet.getString(5));
			User user = new User();
			user.setUserId(resultSet.getInt(7));
			order.setUser(user);
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(VIEW_ORDER_OP);
	}

	public void updateOrder() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from locastrodb.order where order_id= " + order.getOrderId() + ";");

		if (resultSet.next()) {
			preparedStatement = connection.prepareStatement(
					"update locastrodb.order SET quantity = ?, date = ?, price = ?, taxes = ?, status = ?, User_user_id = ? WHERE order_id = ?");
			preparedStatement.setInt(1, order.getQuantity());
			preparedStatement.setDate(2, orderDate);
			preparedStatement.setDouble(3, order.getPrice());
			preparedStatement.setString(4, order.getOrderStatus());
			preparedStatement.setInt(5, order.getUser().getUserId());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(UPDATE_ORDER_OP);
	}

	public void updateOrderStatus() throws SQLException {
			// statements allow to issue SQL queries to the database
			statement = connection.createStatement();
			// resultSet gets the result of the SQL query
			resultSet = statement.executeQuery("select * from locastrodb.order where order_id= " + order.getOrderId() + ";");

			if (resultSet.next()) {
				preparedStatement = connection.prepareStatement(
						"update locastrodb.order SET status = ? where order_id = ?");
				preparedStatement.setString(1, this.orderStatus);
				preparedStatement.setInt(2, order.getOrderId());
				preparedStatement.executeUpdate();
				message.setOpStatus(SUCCESS);
			} else {
				message.setOpStatus(FAILURE);
				message.setErrorMsg("ID value does not exists in the database");
			}
		message.setOpType(UPDATE_ORDER_STATUS_OP);
	}
	
	public void deleteOrder() throws SQLException {

		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from locastrodb.order where order_id = " + order.getOrderId() + ";");

		if (resultSet.next()) {
			preparedStatement = connection.prepareStatement("delete from locastrodb.order where order_id = ?");
			preparedStatement.setInt(1, order.getOrderId());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(DELETE_ORDER_OP);
	}
	
	public void getOrdersByUserId() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select order_id, quantity, date, price, status, User_user_id from locastrodb.order where User_user_id = " + message.getUser().getUserId() + ";");

		while(resultSet.next()) {
			Order order = new Order();
			order.setSelection(Boolean.FALSE);
			order.setOrderId(resultSet.getInt(1));
			order.setQuantity(resultSet.getInt(2));
			order.setOrderDate(resultSet.getDate(3).toLocalDate());
			order.setPrice(resultSet.getDouble(4));
			order.setOrderStatus(resultSet.getString(5));
			User user = new User();
			user.setUserId(resultSet.getInt(6));
			order.setUser(user);
			orderList.add(order);
		} 
		message.setOrderList(orderList);
		message.setOpStatus(SUCCESS);
		message.setOpType(SELECT_ALL_ORDERS_OP);
	}

	public void getAllOrders() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select order_id, quantity, date, price, status, User_user_id from locastrodb.order;");

		while(resultSet.next()) {
			Order order = new Order();
			order.setSelection(Boolean.FALSE);
			order.setOrderId(resultSet.getInt(1));
			order.setQuantity(resultSet.getInt(2));
			order.setOrderDate(resultSet.getDate(3).toLocalDate());
			order.setPrice(resultSet.getDouble(4));
			order.setOrderStatus(resultSet.getString(5));
			User user = new User();
			user.setUserId(resultSet.getInt(6));
			order.setUser(user);
			orderList.add(order);
		} 
		message.setOrderList(orderList);
		message.setOpStatus(SUCCESS);
		message.setOpType(SELECT_ORDERS_OP);	}
	
}
