//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represent the SQL queries for the Payment entity                     *
//*                                                                                                            *
//*                                        Saved in: PaymentSql.java                                           *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.server.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.pizza.data.Message;
import com.pizza.data.Order;
import com.pizza.data.Payment;

public class PaymentSql {
	private Statement statement;
	private ResultSet resultSet;
	private PreparedStatement preparedStatement;
	private Message message;
	private Connection connection;
	private Order order;
	private Payment payment;
	private Date paymentDate;
	private Date cceDate;

	private final String SUCCESS = "S";
	private final String FAILURE = "F";

	private final int VIEW_PAYMENT_OP = 300;
	private final int INSERT_PAYMENT_OP = 310;
	private final int UPDATE_PAYMENT_OP = 320;
	private final int DELETE_PAYMENT_OP = 330;

	public PaymentSql(Connection connection) {
		this.connection = connection;
	}

	public void setParams(Message message) {
		this.message = message;
		if (message.getOpType() == INSERT_PAYMENT_OP) {
			this.payment = message.getPayment();
			this.order = message.getPayment().getOrder();
			
			paymentDate = Date.valueOf(payment.getPaymentDate());
			cceDate = Date.valueOf(payment.getCreditCardExpDate());
		}
		if (message.getOpType() == DELETE_PAYMENT_OP) {
			this.order = message.getOrder();
		}		
	}
	
	public void insertPayment() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from payment where payment_id = " + payment.getPaymentId() + ";");

		if (!resultSet.next()) {
			preparedStatement = connection.prepareStatement("insert into  payment (date, amount, credit_card_number, credit_card_ccv, credit_card_exp_date, taxes, total, Order_order_id) values (?, ?, ?, ?, ?, ?, ?, ?)");
			preparedStatement.setDate(1, paymentDate);
			preparedStatement.setDouble(2, payment.getAmount());
			preparedStatement.setString(3, payment.getCreditCardNumber());
			preparedStatement.setString(4, payment.getCreditCardCcv());
			preparedStatement.setDate(5, cceDate);
			preparedStatement.setDouble(6, payment.getTaxes());
			preparedStatement.setDouble(7, payment.getTotal());
			preparedStatement.setInt(8, order.getOrderId());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID already exists in the database");
		}
		message.setOpType(INSERT_PAYMENT_OP);
	}

	public void viewPayment() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from Payment where payment_id = " + payment.getPaymentId() + ";");

		if (resultSet.next()) {
			payment.setPaymentId(resultSet.getInt(1));
			payment.setPaymentDate(resultSet.getDate(2).toLocalDate());
			payment.setDescription(resultSet.getString(3));
			payment.setAmount(resultSet.getDouble(4));
			payment.setCreditCardNumber(resultSet.getString(5));
			payment.setCreditCardCcv(resultSet.getString(6));
			payment.setCreditCardExpDate(resultSet.getDate(7).toLocalDate());
			payment.setTaxes(resultSet.getDouble(8));
			payment.setTotal(resultSet.getDouble(9));
			Order order = new Order();
			order.setOrderId(resultSet.getInt(10));
			payment.setOrder(order);
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(VIEW_PAYMENT_OP);
	}

	public void updatePayment() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from Payment Where payment_id= " + payment.getPaymentId() + ";");

		if (resultSet.next()) {
			preparedStatement = connection.prepareStatement(
					"update Payment SET date = ?, description = ?, amount = ?, credit_card_number = ?, credit_card_ccv = ?, credit_card_exp_date = ?, taxes = ?, total = ?, Order_order_id = ? WHERE payment_id = ?");
			preparedStatement.setDate(1, paymentDate);
			preparedStatement.setString(2, payment.getDescription());
			preparedStatement.setDouble(3, payment.getAmount());
			preparedStatement.setString(4, payment.getCreditCardNumber());
			preparedStatement.setString(5, payment.getCreditCardCcv());
			preparedStatement.setDate(6, cceDate);
			preparedStatement.setDouble(7, payment.getTaxes());
			preparedStatement.setDouble(8, payment.getTotal());
			preparedStatement.setInt(9, payment.getOrder().getOrderId());
			preparedStatement.setInt(10, payment.getPaymentId());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(UPDATE_PAYMENT_OP);
	}

	public void deletePayment() throws SQLException {

		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select * from payment where Order_order_id = " + order.getOrderId() + ";");

		if (resultSet.next()) {
			preparedStatement = connection.prepareStatement("delete from payment where Order_order_id = ?");
			preparedStatement.setInt(1, order.getOrderId());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database");
		}
		message.setOpType(DELETE_PAYMENT_OP);
	}

}
