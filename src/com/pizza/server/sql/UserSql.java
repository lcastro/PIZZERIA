//**************************************************************************************************************
//*                                                                                                            *
//* CIS611										Spring 2018									Luis Castro		   *
//*                                                                                                            *
//*                                        Program Assignment: Final Project                                   *
//*                                                                                                            *
//*                                        Date Created: 04/27/2018                                            *
//*                                                                                                            *
//*                            Class that represent the SQL queries for the User entity                        *
//*                                                                                                            *
//*                                        Saved in: UserSql.java                                              *
//*                                                                                                            *
//**************************************************************************************************************
package com.pizza.server.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.pizza.data.Message;
import com.pizza.data.User;

public class UserSql {

	private Statement statement;
	private ResultSet resultSet;
	private PreparedStatement preparedStatement;
	private Message message;
	private Connection connection;
	private User user;

	private final String SUCCESS = "S";
	private final String FAILURE = "F";

	private final int RETRIEVE_USR_OP = 100;
	private final int INSERT_USR_OP = 110;
	private final int VALIDATE_USR_OP = 140;

	public UserSql(Connection connection) {
		this.connection = connection;
	}

	public void setParams(Message message) {
		this.message = message;
		this.user = message.getUser();
	}
	
	public void validateLoginCredentials() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select user_id, role, user_name, password, name, mail_address, email_address, phone_number from user where user_name = '" + user.getUserName() + "' and password = '" + user.getPassword() + "' and role = '" + user.getRole() +  "';");

		if (resultSet.next()) {
			user.setUserId(resultSet.getInt(1));
			user.setRole(resultSet.getString(2));
			user.setUserName(resultSet.getString(3));
			user.setPassword(resultSet.getString(4));
			user.setName(resultSet.getString(5));
			user.setMailAddress(resultSet.getString(6));
			user.setEmailAddress(resultSet.getString(7));
			user.setPhoneNumber(resultSet.getString(8));
			message.setUser(user);;
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("Invalid user credentials.");
		}
		message.setOpType(VALIDATE_USR_OP);
	}
	
	public void insertUser() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select user_name from user where user_name = '" + user.getUserName() + "';");

		if (!resultSet.next()) {
			preparedStatement = connection.prepareStatement("insert into user (role, user_name, password, name, mail_address, email_address, phone_number) values (?, ?, ?, ?, ?, ?, ?)");
			preparedStatement.setString(1, user.getRole());
			preparedStatement.setString(2, user.getUserName());
			preparedStatement.setString(3, user.getPassword());
			preparedStatement.setString(4, user.getName());
			preparedStatement.setString(5, user.getMailAddress());
			preparedStatement.setString(6, user.getEmailAddress());
			preparedStatement.setString(7, user.getPhoneNumber());
			preparedStatement.executeUpdate();
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("Username already exists in the database");
		}
		message.setOpType(INSERT_USR_OP);
	}
	
	public void retrieveUser() throws SQLException {
		// statements allow to issue SQL queries to the database
		statement = connection.createStatement();
		// resultSet gets the result of the SQL query
		resultSet = statement.executeQuery("select user_id, role, user_name, password, name, mail_address, email_address, phone_number from user where user_id = " + user.getUserId() + ";");

		if (resultSet.next()) {
			user.setUserId(resultSet.getInt(1));
			user.setRole(resultSet.getString(2));
			user.setUserName(resultSet.getString(3));
			user.setPassword(resultSet.getString(4));
			user.setName(resultSet.getString(5));
			user.setMailAddress(resultSet.getString(6));
			user.setEmailAddress(resultSet.getString(7));
			user.setPhoneNumber(resultSet.getString(8));
			message.setUser(user);;
			message.setOpStatus(SUCCESS);
		} else {
			message.setOpStatus(FAILURE);
			message.setErrorMsg("ID value does not exists in the database.");
		}
		message.setOpType(RETRIEVE_USR_OP);
	}
	
}
